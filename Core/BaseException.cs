﻿using System;

namespace Core
{
    public class BaseException : Exception
    {
        public string ErrorCode { get; set; }

        public BaseException(string message)
            : base(message)
        {
        }
    }
}