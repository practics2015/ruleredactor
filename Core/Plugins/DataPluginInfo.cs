﻿using System;

namespace Core.Plugins
{
    class DataPluginInfo
    {
        public Type Type { get; set; }
        public IDataCreator Creator { get; set; }
        public IDataSerializer Serializer { get; set; }
    }
}