﻿using System;
using System.Reflection;

namespace Core.Plugins
{
    public class DataCreator<T> : IDataCreator<T>
        where T : IData
    {
        public T Create()
        {
            return DoCreate();
        }

        protected virtual T DoCreate()
        {
            return Activator.CreateInstance<T>();
        }

        IData IDataCreator.Create()
        {
            return Create();
        }
    }
}