﻿using System;
using System.Xml.Serialization;

namespace Core.Plugins
{
    [Serializable]
    public class Data<T> : IData
    {
        [XmlElement]
        public T Value { get; set; }

        [XmlElement]
        public string Version { get; set; }
    }
}