﻿namespace Core.Plugins
{
    

    public interface IDataCreator
    {
        IData Create();
    }

    public interface IDataCreator<T> : IDataCreator
        where T : IData
    {
        new T Create();
    }
}