﻿using System;
using System.Collections.Generic;
using NLog;

namespace Core.Plugins
{
    /// <summary>
    /// Реестр данных
    /// </summary>
    public class DataRegistry
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();


        static Dictionary<Type, DataPluginInfo> dataPlugins = new Dictionary<Type, DataPluginInfo>();

        public static void RegisterData(Type dataType, IDataCreator dataCreator, IDataSerializer dataSerizlizer)
        {
            var dataPlugin = new DataPluginInfo() { Type = dataType, Creator = dataCreator, Serializer = dataSerizlizer };

            dataPlugins[dataType] = dataPlugin;
            logger.Info("Данные {0} зарегистрированы", dataType.FullName);
        }

        public static void RegisterData<T>()
             where T : class, IData
        {
            RegisterData(typeof (T), new DataCreator<T>(), new DataSerializer<T>());
        }

        public static T GetDataCreator<T>(Type dataType)
            where T : class, IDataCreator
        {
            return dataPlugins[dataType].Creator as T;
        }

        public static T GetDataSerializer<T>(Type dataType)
            where T : class, IDataSerializer
        {
            return dataPlugins[dataType].Serializer as T;
        }

        public static IDataSerializer GetDataSerializer(Type dataType)
        {
            return dataPlugins[dataType].Serializer;
        }


    }
}