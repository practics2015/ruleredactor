﻿using System.Collections.Generic;

namespace Core.Plugins
{
    public interface IDataList<T> : IData, IList<T>
    {
        
    }
}