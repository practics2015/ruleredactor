﻿namespace Core.Plugins
{
    public interface IDataSerializer
    {
        string Serialize(IData data);
        IData Deserialize(string serializedData);
    }

    public interface IDataSerializer<T> : IDataSerializer
        where T : class, IData
    {
        string Serialize(T data);
        new T Deserialize(string serializedData);
    }

    public class DataSerializer<T> : IDataSerializer<T>
        where T : class, IData
    {
        public string Serialize(T data)
        {
            return Serializer.SerializeObjectToString(data);
        }
        
        public T Deserialize(string serializedData)
        {
            return Serializer.DeserializeObject<T>(serializedData);
        }

        string IDataSerializer.Serialize(IData data)
        {
            return Serialize(data as T);
        }

        IData IDataSerializer.Deserialize(string serializedData)
        {
            return Deserialize(serializedData);
        }

    }
}