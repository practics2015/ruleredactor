﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NLog;

namespace Core.Plugins
{
    public class PluginActivator
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Activate(PluginSettings settings)
        {
            OnSendMessage("Идет процесс сборки плагинов");
            //Splash.ShowStatus("Идет процесс сборки плагинов");
            var files = new List<string>();
            
            // ищем все сборки в директориях с плагинами
            foreach (var pluginDirectory in settings.PluginDirectories)
            {
                try
                {
                    var dir = pluginDirectory;

                    if (string.IsNullOrEmpty(pluginDirectory))
                        dir = Path.GetDirectoryName(Application.ExecutablePath);



                    if (!Directory.Exists(dir))
                    {
                        logger.Warn("Указанная директория с плагинами {0} не найдена", pluginDirectory);
                        continue;
                    }

                    var fullPath = Path.GetFullPath(dir);

                    files.AddRange(Directory.GetFiles(fullPath, "*.exe"));
                    files.AddRange(Directory.GetFiles(fullPath, "*.dll"));
                }
                catch (Exception e)
                {
                    logger.Error("Ошибка поиска директории {0}: {1}", pluginDirectory, e.ToString());
                }
            }

            ActivatePluginsFromFiles(files);
        }


        static void ActivatePluginsFromFiles(IEnumerable<string> files)
        {
            foreach (var file in files)
            {
                if (string.IsNullOrEmpty(file) || Path.GetFileName(file).StartsWith("DevExpress"))
                    continue;

                var pluginTypes = GetExportedTypes(file);

                foreach (var pluginType in pluginTypes)
                {
                    try
                    {

                        Install(pluginType);
                    }
                    catch (Exception e)
                    {
                        logger.Error("Ошибка при инсталляции плагина {0}: {1}", pluginType.FullName, e.ToString());
                    }
                }
            }
        }

        private static IEnumerable<Type> GetExportedTypes(string filePath)
        {
            var res = new List<Type>();

            try
            {
                var assembly = Assembly.LoadFile(filePath);
                res.AddRange(assembly.GetExportedTypes());
            }
            catch (Exception e)
            {
                logger.Error("Ошибка при поиске плагинов в файле {0}: {1}", filePath, e.ToString());
            }

            return res;
        }

        static void Install(Type pluginType)
        {
            var methods = pluginType.GetMethods(BindingFlags.Static | BindingFlags.Public);

            foreach (var method in methods)
            {
                var attr = (InstallAttribute)method.GetCustomAttribute(typeof(InstallAttribute));

                if (attr != null)
                {
                    logger.Info("Найден плагин {0}. Процедура инсталляции начата", pluginType.FullName);
                    method.Invoke(null, null);
                    logger.Info("Плагин {0} успешно инсталлирован", pluginType.FullName);
                    
                    // TODO: для демонстрации, потом удалить
                    //Thread.Sleep(500);
                    ///
                    
                    var message = string.Format("Плагин {0} успешно инсталлирован", pluginType.FullName);
                    OnSendMessage(message);
                    
                    //Splash.ShowStatus(message);
                }
            }
        }


        public static event EventHandler<string> SendMessage;

        private static void OnSendMessage(string e)
        {
            var handler = SendMessage;
            if (handler != null) handler(null, e);
        }
    }
}
