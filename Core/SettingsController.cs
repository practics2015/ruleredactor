﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using NLog;

namespace Core
{
    public class SettingsController<T>
        where T : class, new()
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static string settingsPath;

        private static T _settings;
        public static T Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }
        
        static SettingsController()
        {
            _settings = new T();
            //settingsPath = Path.Combine(Application.CommonAppDataPath, _settings.ToString() + ".xml");
            settingsPath = Path.Combine(Application.UserAppDataPath, _settings.ToString() + ".xml");
        }

        public static void SaveSettings()
        {
            SaveSettings(settingsPath);
        }

        public static void SaveSettings(string path)
        {
            Serializer.SerializeObject<T>(path, _settings);
        }

        public static void LoadSettings()
        {
            LoadSettings(settingsPath);
        }

        public static void LoadSettings(string path)
        {
            _settings = new T();

            if (!File.Exists(path))
                return;
            

            try
            {
                using (XmlReader xmlReader = XmlReader.Create(path))
                {
                    _settings = Serializer.DeserializeObject<T>(xmlReader);
                }
            }
            catch(Exception e)
            {
                logger.Error("Ошибка загрузки настроек: {0}", e.ToString());
            }
        }

    }
}
