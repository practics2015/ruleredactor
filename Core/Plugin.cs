﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification.Data;
using Core.Plugins;

namespace Core
{
    public class Plugin
    {
        [Install]
        public static void Install()
        {
            DataRegistry.RegisterData(typeof(UserData), new DataCreator<UserData>(), new DataSerializer<UserData>());
            DataRegistry.RegisterData(typeof(RuleListData), new DataCreator<RuleListData>(), new DataSerializer<RuleListData>());
        }
    }
}
