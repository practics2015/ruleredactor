﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Core.Plugins;

namespace Core
{
    [Serializable]
    public class CoreConfig
    {
        public CoreConfig()
        {
            PluginSettings = new PluginSettings();
            AddDefaultPluginDirectories();
            ServerAddress = "rabbitmq://localhost/MainServer";

        }

        void AddDefaultPluginDirectories()
        {
            var executableDirectory = Path.GetDirectoryName(Application.ExecutablePath);

            if (string.IsNullOrEmpty(executableDirectory))
                executableDirectory = string.Empty;

            var pluginDirectory = Path.Combine(executableDirectory, "Plugins");

            PluginSettings.PluginDirectories.Add(pluginDirectory);
        }

        [XmlElement]
        public PluginSettings PluginSettings { get; set; }

        [XmlElement]
        public string ServerAddress { get; set; }

    }
}
