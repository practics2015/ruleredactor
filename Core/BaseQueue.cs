﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class BaseQueue<T> : Queue<T>
        where T: class 
    {
        Queue<T> queue = new Queue<T>();

        public BaseQueue()
        {
        }

        public new void Enqueue(T message)
        {
            queue.Enqueue(message);

            if(ItemEnqueue!=null)
                ItemEnqueue(this, new BaseEventArgs<T>(message));
        }
        
        public new T Dequeue()
        {
            if (queue.Count <=0)
                return null;

            return queue.Dequeue();
        }

        public void Dispose()
        {
            queue.Clear();
        }

        public event EventHandler<BaseEventArgs<T>> ItemEnqueue;

    }
}
