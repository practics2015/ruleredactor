using System;

namespace Core
{
    public class BaseEventArgs<T> : EventArgs
    {
        public T Value;

        public BaseEventArgs(T value)
        {
            Value = value;
        }
    }
}