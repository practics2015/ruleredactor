﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    [XmlInclude(typeof(RuleService))]
    [XmlInclude(typeof(RuleData))]
    public class Rule
    {
        /// <summary>
        /// Уникальный идентификатор правила. Для сервисов это имя метода сервиса. Для данных это тип данных.
        /// </summary>
        [XmlElement]
        public string RuleId { get; set; }

        /// <summary>
        /// Проверяет равны ли значения названий этого права и права на входе
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Equals(Rule rule)
        {
            return Name == rule.Name;
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Rule p = obj as Rule;
            if ((System.Object)p == null)
            {
                return false;
            }

            return (Name == p.Name);
        }
        

        /// <summary>
        /// Отображаемое имя правила
        /// </summary>
        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public string Description { get; set; }
       
    }
}