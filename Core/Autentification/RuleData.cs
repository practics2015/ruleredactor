﻿using System;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class RuleData : Rule
    {
        public RuleData()
        {
            Description = "Правило доступа к данным";
        }


        private string _dataType;
        /// <summary>
        ///  Тип данных право на который дает этот объект
        /// </summary>
        [XmlElement]
        string DataType
        {
            get { return _dataType; }
            set
            {
                _dataType = value;
                this.RuleId = _dataType;
            }
        }

    }
}