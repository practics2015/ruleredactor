﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Autentification
{
    class GroupAlreadyExistException : BaseException
    {
        public GroupAlreadyExistException(string name)
            : base(string.Format("Группа с именем {0} уже существует в системе.", name))
        {
            ErrorCode = "000540";
        }
    }
}
