﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Autentification
{
    class UserAlreadyExistException : BaseException
    {
        public UserAlreadyExistException(string samUserName)
            : base(string.Format("Пользователь {0} уже зарегистрирован в системе.", samUserName))
        {
            ErrorCode = "000530";
        }
    }
}
