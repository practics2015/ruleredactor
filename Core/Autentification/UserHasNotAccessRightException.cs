﻿namespace Core.Autentification
{
    public class UserHasNotAccessRightException : BaseException
    {
        public UserHasNotAccessRightException(string samUserName, string ruleKey)
            : base(string.Format("Пользователь {0} не имеет прав доступа к объекту {1}", samUserName, ruleKey))
        {
            ErrorCode = "000520";
        }
    }
}