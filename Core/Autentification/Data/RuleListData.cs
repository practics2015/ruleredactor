﻿using System;
using System.Collections.Generic;
using Core.Plugins;

namespace Core.Autentification.Data
{
    /// <summary>
    /// Объект данных для передачи списка прав доступных пользователю
    /// </summary>
    [Serializable]
    public class RuleListData : Data<List<Rule>>
    {


    }
}
