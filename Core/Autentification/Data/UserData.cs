﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Plugins;

namespace Core.Autentification.Data
{
    /// <summary>
    /// Объект данных для передачи данных пользователя
    /// </summary>
    [Serializable]
    public class UserData : Data<User>
    {
    }
}
