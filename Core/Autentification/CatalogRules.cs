﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class CatalogRules 
    {
        [XmlElement]
        public List<Rule> Rules = new List<Rule>();

        /// <summary>
        /// Добавляет право в каталог прав
        /// </summary>
        /// <param name="rule"></param>
        public void AddRule(Rule rule)
        {
            if(!Rules.Contains(rule))
            Rules.Add(rule);
        }

        /// <summary>
        /// Возвращает объет права по его названию
        /// </summary>
        /// <param name="ruleName"></param>
        /// <returns></returns>
        public Rule GetRule(string ruleName)
        {
            foreach (var rule in Rules)
            {
                if (rule.Name.Equals(ruleName))
                    return rule;
            }

            return null;
        }

        /// <summary>
       /// Удаляет объект права по его названию
       /// </summary>
       /// <param name="ruleName"></param>
        public void RemoveRule(string ruleName)
        {
            foreach (var rule in Rules)
            {
                if (rule.Name.Equals(ruleName))
                    Rules.Remove(rule);
            }
        }

        /// <summary>
        /// Удаляет соотвествующий объект права
        /// </summary>
        /// <param name="rule"></param>
        public void RemoveRule(Rule rule)
        {
            if(Rules.Contains(rule))
            Rules.Remove(rule);
        }

        /// <summary>
        /// Позволяет получить список всех прав в каталоге
        /// </summary>
        /// <returns></returns>
        public List<Rule> GetAllRules()
        {
            return new List<Rule>(Rules);
        }

        /// <summary>
        /// Изменяет название указанного права в каталоге прав
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="newName"></param>
        public void ChangeRule(Rule rule, string newName, string newDescription)
        {
            if (Rules.Contains(rule))
            {
                if (newName != null && newName != "")
                {
                    rule.Name = newName;
                    rule.Description = newDescription;
                }
                //else
                    //throw Exception
            }
            //else
                //throw RuleNotFoundException(rule.Name);
        }
    }
}
