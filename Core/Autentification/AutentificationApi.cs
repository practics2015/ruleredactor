﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Autentification
{
    public class AutentificationApi
    {
        public static IActiveDirectorySettings GetActiveDirectorySettings()
        {
            return ServiceLocator.GetService<IActiveDirectorySettings>();
        }

    }
}
