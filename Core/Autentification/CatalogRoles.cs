﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class CatalogRoles
    {
        [XmlElement]
        public List<Role> Roles = new List<Role>();

        /// <summary>
        /// Добавляет роль в список ролей
        /// </summary>
        /// <param name="role"></param>
        public void AddRole(Role role)
        {
            if(!Roles.Contains(role))
            Roles.Add(role);
            //else
            //throw new RoleAlreadyExistException(Role.Name);
        }

        /// <summary>
        /// Возвращает объект роли по ее названию
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public Role GetRole(string roleName)
        {
            foreach (var role in Roles)
            {
                if (role.Name.Equals(roleName))
                    return role;
            }

            return null;
        }

        /// <summary>
       /// Удаляет роль по ее названию
       /// </summary>
       /// <param name="roleName"></param>
        public void RemoveRole(string roleName)
        {
            foreach (var role in Roles)
            {
                if (role.Name.Equals(roleName))
                    Roles.Remove(role);
            }
        }

        /// <summary>
        /// Удаляет роль из списка ролей
        /// </summary>
        /// <param name="role"></param>
        public void RemoveRole(Role role)
        {
            if(Roles.Contains(role))
            Roles.Remove(role);
        }

        /// <summary>
        /// Удаляет право из ролей из каталога ролей
        /// </summary>
        /// <param name="rule"></param>
        public void RemoveRule(Rule rule)
        {
            foreach (var role in Roles)
            {
                role.RemoveRule(rule);
            }
        }

        /// <summary>
        /// Изменяет название указанной роли из каталога ролей
        /// </summary>
        /// <param name="role"></param>
        /// <param name="newName"></param>
        public void ChangeRoleName(Role role, string newName)
        {
            if (Roles.Contains(role))
            {
                if(newName != null && newName != "")
                role.Name = newName;
                //else
                    //throw new Exception
            }
            //else
                //throw RoleNotFoundException(role.Name);
        }
    }
}