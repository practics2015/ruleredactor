﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class UserGroup
    {
        public UserGroup()
        {
            Users = new List<User>();
            Roles = new List<Role>();
        }

        /// <summary>
        /// Уникальный идентификатор группы. Пока будем использовать в его качестве имя группы
        /// </summary>
        [XmlElement]
        public string Id {
            get { return _name; }
            set
            {
                _name = value;
            }
        }

        private string _name;

        [XmlElement]
        public string Name
        {
            get { return _name; }
            set 
            { 
                _name = value;
            }
        }

        [XmlElement]
        public List<User> Users { get; set; }
        
        [XmlElement]
        public List<Role> Roles { get; set; }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            UserGroup p = obj as UserGroup;
            if ((System.Object)p == null)
            {
                return false;
            }

            return (Name == p.Name);
        }


        public bool Equals(UserGroup userGroup)
        {
            return Name == userGroup.Name;
        }



        /// <summary>
        /// Добавление пользователя в группу
        /// </summary>
        /// <param name="user"></param>
        public void AddUser(User user)
        {
            if (!Users.Contains(user))
            Users.Add(user);
        }

        /// <summary>
        /// Функция возвращает правила для группы
        /// </summary>
        /// <returns></returns>
        public ICollection<Rule> GetRules()
        {
            var res = new List<Rule>();
            foreach (var role in Roles)
            {
                res.AddRange(role.Rules);
            }
            return res;
        }


        /// <summary>
        /// Удаление пользователя по его логину
        /// </summary>
        /// <param name="samUserName"></param>
        public void RemoveUser(string samUserName)
        {
            if (!string.IsNullOrEmpty(samUserName))
            {
                foreach (var user in Users)
                {
                    if (user.DisplayName.ToLower().Equals(samUserName.ToLower()))
                        Users.Remove(user);
                }
            }
            throw new UserNotFoundException(samUserName);
        }

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="user"></param>
        public void RemoveUser(User user)
        {
            if(Users.Contains(user))
            Users.Remove(user);
        }

        /// <summary>
        /// Удаление роли по ее названию
        /// </summary>
        /// <param name="RoleName"></param>
        public void RemoveRole(string RoleName)
        {
            foreach(var role in Roles)
            {
                if (role.Name.Equals(RoleName))
                    Roles.Remove(role);
            }
        }
        
        /// <summary>
        /// Удаляет роль из списка ролей группы
        /// </summary>
        /// <param name="role"></param>
        public void RemoveRole(Role role)
        {
            if (Roles.Contains(role))
                Roles.Remove(role);
        }

        public void AddRole(Role role)
        {
            if(!Roles.Contains(role))
            Roles.Add(role);
            foreach(var user in Users)
            user.AddRole(role);
        }
    }
}
