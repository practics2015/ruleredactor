﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class CatalogUserGroups
    {
        public CatalogUserGroups()
        {
            Groups = new List<UserGroup>();
        }


        [XmlElement]
        public List<UserGroup> Groups { get; set; }


        /// <summary>
        /// Добавляет новую групппу в спсок групп (если группа с таким именем уже существует выдается ошибка)
        /// </summary>
        /// <param name="group"></param>
        public void AddUserGroup(UserGroup group)
        {
            // TODO: подготовка данных для отладки механизма аутентификации
            //if (group.Name.Equals("Programmers"))
            //{
            //    group.Roles.Add(new Role() { Name = "test", Rules = new List<Rule>() { new RuleService() { ServiceName = "ServerApplication.LongerProcess", Name = "ServerApplication.LongerProcess", RuleId = "ServerApplication.LongerProcess" } } });
            //}

            if(!Groups.Contains(group))
            Groups.Add(group);
            else
            throw new GroupAlreadyExistException(group.Name);
        }

        /// <summary>
        /// Удаляет группу из каталога групп
        /// </summary>
        /// <param name="group"></param>
        public void RemoveUserGroup(UserGroup group)
        {
            if(Groups.Contains(group))
            Groups.Remove(group);
        }

        /// <summary>
        /// Ищет в каталоге группы пользователей в которые входит пользователь с userId
        /// </summary>
        /// <returns></returns>
        public ICollection<UserGroup> GetUserGroupsByUserId(string userId)
        {
            var res = new List<UserGroup>();

            foreach (var group in Groups)
            {
                foreach (var user in group.Users)
                {
                    if (user.Id.ToLower().Equals(userId.ToLower()))
                    {
                        res.Add(group);
                    }
                }

                res.Add(group);
            }

            return res;
        }

        /// <summary>
        /// Ищет в каталоге группы пользователей в которые входит пользователь с 
        /// </summary>
        /// <returns></returns>
        public ICollection<UserGroup> GetUserGroupsByUserName(string samUserName)
        {
            var res = new List<UserGroup>();

            foreach (var group in Groups)
            {
                foreach (var user in group.Users)
                {
                    if (user.SamName.ToLower().Equals(samUserName.ToLower()))
                    {
                        res.Add(group);
                    }
                }

                res.Add(group);
            }

            return res;
        }

        public UserGroup GetUserGroupByGroupName(string groupName)
        {
            foreach (var userGroup in Groups)
            {
                if (userGroup.Name.ToLower().Equals(groupName.ToLower()))
                    return userGroup;
            }

            return null;
        }

        /// <summary>
        /// Удаляет группу пользователей из каталога, по ее имени
        /// </summary>
        /// <param name="groupName"></param>
        public void RemoveUserGroupByGroupName (string groupName)
        {
            groupName = groupName.ToLower();
            foreach (var userGroup in Groups)
            {
                if (userGroup.Name.ToLower().Equals(groupName))
                {
                    Groups.Remove(userGroup);
                    return;
                }
            }
        }

        /// <summary>
        /// Удаляет роль из списка ролей все групп пользователей в каталоге
        /// </summary>
        /// <param name="role"></param>
        public void RemoveRole(Role role)
        {
            foreach (var group in Groups)
            {
                group.RemoveRole(role);
            }
        }

        /// <summary>
        /// Изменяет название указанной группы пользователей из каталога
        /// </summary>
        /// <param name="userGroup"></param>
        /// <param name="newName"></param>
        public void ChangeGroupName(UserGroup userGroup, string newName)
        {
            if (Groups.Contains(userGroup))
            {
                if (newName != null && newName != "")
                userGroup.Name = newName;
                //else
                    //throw new Exception
            }
             //else
                //throw new UserGroupNotFoundException(userGroup.Name);
        }
    }
}