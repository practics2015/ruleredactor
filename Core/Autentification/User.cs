﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class User
    {
        public User()
        {
            Roles = new List<Role>();
            UserGroups = new List<UserGroup>();
        }


        /// <summary>
        /// Проверяет равны ли значения имени этого пользователя и пользователя на входе
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Equals(User user)
        {
            return SamName == user.SamName;
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            User p = obj as User;
            if ((System.Object)p == null)
            {
                return false;
            }

            return (SamName == p.SamName);
        }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        [XmlElement]
        public string SamName { get; set; }

        /// <summary>
        /// Отображаемое имя пользователя
        /// </summary>
        [XmlElement]
        public string DisplayName { get; set; }

        public override string ToString()
        {
            return SamName + " " + DisplayName;
        }

        [XmlElement]
        public List<Role> Roles { get; set; }

        [XmlElement]
        public List<UserGroup> UserGroups { get; set; }

        [XmlElement]
        public string Id { get; set; }

        /// <summary>
        /// Функция генерации уникального ключа для пользователя
        /// </summary>
        /// <returns></returns>
        public static string GenerateUserId()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Функция выбирает все права пользователя. 
        /// </summary>
        /// <returns></returns>
        public ICollection<Rule> GetUserRules()
        {
            var userRules = new List<Rule>();

            foreach (var role in Roles)
            {
                userRules.AddRange(role.Rules);
            }

            var userGroupRules = new List<Rule>();

            foreach (var userGroup in UserGroups)
            {
                userGroupRules.AddRange(userGroup.GetRules());
            }

            // ищем только уникальные правила
            // <rule.RuleId, rule
            var resRules = new Dictionary<string, Rule>();

            foreach (var userRule in userRules)
            {
                resRules[userRule.RuleId] = userRule;
            }

            foreach (var userRule in userGroupRules)
            {
                resRules[userRule.RuleId] = userRule;
            }

            return new List<Rule>(resRules.Values);
        }

        /// <summary>
        /// Удаляет роль пользователя, по названию роли
        /// </summary>
        /// <param name="roleName"></param>
        public void RemoveRole(string roleName)
        {
            roleName = roleName.ToLower();
            foreach (var role in Roles)
            {
                if (role.Name.Equals(roleName))
                    Roles.Remove(role);
            }
        }
       
        public UserGroup FindGroupByName(string groupName)
        {
            var GroupName = groupName.ToLower();
            foreach (var userGroup in UserGroups)
            {
                if (userGroup.Name.ToLower().Equals(GroupName))
                    return userGroup;
            }

            return null;
        }

        /// <summary>
        /// Добавляет группу в список групп, к которым принадлежит пользователь
        /// </summary>
        /// <param name="group"></param>
        public void AddUserGroup(UserGroup group)
        {
            if (!UserGroups.Contains(group))
                UserGroups.Add(group);
        }

        /// <summary>
        /// Удаляет группу из списка групп, к которым принадлежит пользователь
        /// </summary>
        /// <param name="group"></param>
        public void RemoveUserGroup(UserGroup group)
        {
            if (UserGroups.Contains(group))
                UserGroups.Remove(group);
        }

        /// <summary>
        /// Удаляет группу из списка групп, к которым принадлежит пользователь по названию группы
        /// </summary>
        /// <param name="groupName"></param>
        public void RemoveUserGroup(string groupName)
        {

            foreach (var userGroup in UserGroups)
            {
                groupName = groupName.ToLower();
                if (userGroup.Name.ToLower().Equals(groupName))
                {
                    UserGroups.Remove(userGroup);
                    return;
                }
            }
        }

        /// <summary>
        /// Добавляет роль в список ролей пользователя
        /// </summary>
        /// <param name="role"></param>
        public void AddRole(Role role)
        {
            if (!Roles.Contains(role))
            Roles.Add(role);
        }

        /// <summary>
        /// Удаляет роль из списка ролей пользователя
        /// </summary>
        /// <param name="role"></param>
        public void RemoveRole(Role role)
        {
            if (Roles.Contains(role))
                Roles.Remove(role);
        }

    }
}