﻿namespace Core.Autentification
{
    public class UserGroupNotFoundException : BaseException
    {
        public UserGroupNotFoundException(string id)
            : base(string.Format("Группа пользователей {0} не зарегистрирована в системе.", id))
        {
            ErrorCode = "000510";
        }
    }
}