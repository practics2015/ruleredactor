﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class RuleService : Rule
    {
        public RuleService()
        {
            Description = "Правило доступа к сервису";
        }


        private string _serviceName;
        /// <summary>
        ///  Имя сервиса право на который дает этот объект
        /// </summary>
        [XmlElement]
        public string ServiceName
        {
            get { return _serviceName; }
            set
            {
                _serviceName = value;
                this.RuleId = _serviceName;
            }
        }

    }
}