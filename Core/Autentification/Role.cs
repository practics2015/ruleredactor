﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class Role
    {
        public Role()
        {
            Rules = new List<Rule>();
        }

        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public List<Rule> Rules { get; set; }

        /// <summary>
        /// Проверяет равны ли значения названий этой роли и роли на входе
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool Equals(Role role)
        {
            return Name == role.Name;
        }

        public override bool Equals(System.Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Role p = obj as Role;
            if ((System.Object)p == null)
            {
                return false;
            }

            return (Name == p.Name);
        }

        /// <summary>
        /// Добавление права в список прав у роли
        /// </summary>
        /// <param name="rule"></param>
        public void AddRule(Rule rule)
        {
            if(!Rules.Contains(rule))
            Rules.Add(rule);
        }

        /// <summary>
        /// Удаляет право из списка прав для роли
        /// </summary>
        /// <param name="rule"></param>
        public void RemoveRule(Rule rule)
        {
            if (Rules.Contains(rule))
            Rules.Remove(rule);
        }

    }
}