﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Core.Autentification
{
    [Serializable]
    public class CatalogUsers
    {
        public CatalogUsers()
        {
            Users = new List<User>();
        }


        [XmlElement]
        public List<User> Users { get; set; }



        /// <summary>
        /// Добавляет нового пользователя в каталог
        /// </summary>
        /// <param name="Пользователь"></param>
        public void AddUser(User user)
        {
            if (!Users.Contains(user))
                Users.Add(user);
            else
                throw new UserAlreadyExistException(user.SamName); 
        }


        /// <summary>
        /// Добавление пользователя в группу пользователей
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userGroup"></param>
        public void AddUserToGroup(User user, UserGroup userGroup)
        {
            if (!user.UserGroups.Contains(userGroup))
            user.UserGroups.Add(userGroup);
        }

        /// <summary>
        /// Ищет в каталоге пользователей пользователя по логину
        /// </summary>
        /// <returns></returns>
        public User GetUser(string samUserName)
        {
            if (!string.IsNullOrEmpty(samUserName))
            {
                foreach (var user in Users)
                {
                    if (user.SamName.ToLower().Equals(samUserName.ToLower()))
                        return user;
                }
            }

            throw new UserNotFoundException(samUserName);
        }

        /// <summary>
        /// Удаляет пользователя по его логину
        /// </summary>
        /// <param name="Логин пользователя"></param>
        public void RemoveUser(string samUserName)
        {
            if (!string.IsNullOrEmpty(samUserName))
            {
                foreach (var user in Users)
                {
                    if (user.SamName.ToLower().Equals(samUserName.ToLower()))
                        Users.Remove(user);
                }
            }

            throw new UserNotFoundException(samUserName);
        }

        /// <summary>
        /// Удаляет пользователя из каталога
        /// </summary>
        /// <param name="Пользователь"></param>
        public void RemoveUser(User user)
        {
            if(Users.Contains(user))
            Users.Remove(user);
        }

        /// <summary>
        /// Удаление группы пользователей из списка групп и пользователя
        /// </summary>
        /// <param name="userGroup"></param>
        public void RemoveUserGroup(UserGroup userGroup)
        {
            foreach (var user in Users)
            {
                user.RemoveUserGroup(userGroup);
            }
        }

        /// <summary>
        /// Ищет в каталоге зарегистрированных пользователей пользователя по логину
        /// </summary>
        /// <returns></returns>
        public User GetUserByUserId(string userId)
        {
            foreach (var user in Users)
            {
                // проверка зарегистрирован ли пользователь
                if (string.IsNullOrEmpty(user.Id) || string.IsNullOrEmpty(userId))
                    continue;

                if (user.Id.ToLower().Equals(userId.ToLower()))
                    return user;
            }

            throw new UserNotFoundException(userId);
        }

        /// <summary>
        /// Удаляет роль из списка ролей всех пользователей в каталоге
        /// </summary>
        /// <param name="role"></param>
        public void RemoveRole(Role role)
        {
            foreach(var user in Users)
            {
                user.RemoveRole(role);
            }
        }

        /// <summary>
        /// Изменяет логин и описание указанного пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        public void ChangeUserNames(User user, string newSamName, string newDisplayName)
        {
            if (Users.Contains(user))
            {
                if (newSamName != null && newSamName != "")
                {
                    user.SamName = newSamName;
                    user.DisplayName = newDisplayName;
                }
                //else
                    //throw new UserNameIsNullException();
            }
            //else
                //throw new UserNotFountException(user.SamName);
        }
    }
}