﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public interface IActiveDirectorySettings
    {
        /// <summary>
        /// Имя домена
        /// </summary>
        string Domain { get; set; }

        /// <summary>
        /// Логин админа домена
        /// </summary>
        string AdminLogin { get; set; }

        /// <summary>
        /// Пароль админа домена
        /// </summary>
        string AdminPassword { get; set; }

    }
}
