﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Plugins;
using NLog;

namespace Core
{
    public class CoreActivator
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static CoreActivator()
        {
            CoreConfigPath = "CoreConfig.xml";
        }

        public static string CoreConfigPath { get; set; }

        public static void Activate()
        {
            try
            {
                OnActivateMessage("Загружаем настройки.");
                //Splash.ShowStatus("Загружаем настройки.");
                SettingsController<CoreConfig>.LoadSettings(CoreConfigPath);
                var coreConfig = SettingsController<CoreConfig>.Settings;

                PluginActivator.SendMessage += PluginActivator_SendMessage;
                PluginActivator.Activate(coreConfig.PluginSettings);
            }
            catch (Exception e)
            {
                logger.Error("Ошибка сборки плагинов: {0}", e.ToString());
            }
            finally
            {
                PluginActivator.SendMessage -= PluginActivator_SendMessage;
            }
        }

        static void PluginActivator_SendMessage(object sender, string e)
        {
            OnActivateMessage(e);
        }

        public static event EventHandler<string> ActivateMessage;

        private static void OnActivateMessage(string e)
        {
            var handler = ActivateMessage;
            if (handler != null) handler(null, e);
        }
    }
}
