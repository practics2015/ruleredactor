﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Core
{
    /// <summary>
    /// Сериализация и десериализация 
    /// </summary>
    public class Serializer
    {
        /// <summary>
        /// Десериализовать объект из Xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string xml)
            where T : class
        {
            var ser = new XmlSerializer(typeof(T));
            T res = null;

            var bytes = Encoding.UTF8.GetBytes(xml);
            using (var memoryStream = new MemoryStream(bytes))
            {
                res = (T)ser.Deserialize(memoryStream);
            }
            return res;
        }

        /// <summary>
        /// Десериализовать объект из Xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(XmlReader xmlReader)
            //where T : class
        {
            var ser = new XmlSerializer(typeof(T));
            T res = (T)ser.Deserialize(xmlReader);
            return res;
        }

        /// <summary>
        /// Сериализовать объект в xml
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializeFileName"></param>
        /// <param name="object"></param>
        public static void SerializeObject<T>(string serializeFileName, T obj)
        {
            try
            {
                var ser = new XmlSerializer(typeof(T));

                using (var f = new FileStream(serializeFileName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    using (TextWriter writer = new StreamWriter(f, Encoding.UTF8))
                    //using (TextWriter writer = new StreamWriter(f, Encoding.GetEncoding(1251)))
                    {
                        ser.Serialize(writer, obj);
                    }
                }

            }
            catch (Exception)
            {
                // надо разобраться почему может быть отказ прав доступа к общедоступной директории пользователей
            }
        }


        public static string SerializeObjectToString<T>(T obj)
        {
            var ser = new XmlSerializer(typeof(T));

            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriterWithEncoding(sb, Encoding.UTF8))
            //using (TextWriter writer = new StringWriterWithEncoding(sb, Encoding.GetEncoding(1251)))
            {
                ser.Serialize(writer, obj);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Возвращает MemoryStream для строки xml
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static MemoryStream CreateMemoryStream(string xml)
        {
            var memoryStream = new MemoryStream((byte[])StringToByteArray(xml));
            return memoryStream;
        }

        /// <summary>
        /// Конвертирует xmlString в массив байтов в кодировке 
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public static Byte[] StringToByteArray(string xmlString)
        {
            var encoding = new UTF8Encoding();
            //var encoding = Encoding.GetEncoding(1251);
            

            var byteArray = encoding.GetBytes(xmlString);
            return byteArray;
        }

        public static T CopyObject<T>(T obj) 
            where T: class 
        {
            var xml = SerializeObjectToString<T>(obj);
            return DeserializeObject<T>(xml);
        }
    }
}
