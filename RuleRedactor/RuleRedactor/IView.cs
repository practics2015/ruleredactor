﻿using System;
using Core.Autentification;
using System.Collections.Generic;

namespace RuleRedactor
{
     interface IView
    {
        // !!важно добавить в код интерфейса реализацию всех методов отсюда, иначе будет выдавать ошибку
        event EventHandler<PathEventArgs> ButtonLoadClick;

        event EventHandler<PathEventArgs> ButtonSaveClick;

            


       // события удаления

        event EventHandler<UsersEventArgs> ButtonDeleteUserClick;   
        event EventHandler<GroupsEventArgs> ButtonDeleteGroupClick;      
        event EventHandler<RulesEventArgs> ButtonDeleteRuleClick;       
        event EventHandler<RolesEventArgs> ButtonDeleteRoleClick;

        //события добавления в список

        event EventHandler<UsersGroupsEventArgs> ButtonAddUserToGroupClick;
        event EventHandler<RolesGroupsEventArgs> ButtonAddRoleToGroupClick;          
        event EventHandler<RolesUsersEventArgs> ButtonAddRoleToUserClick;            
        event EventHandler<RulesRolesEventArgs> ButtonAddRuleToRoleClick;                

        //события удаления из списка
        
        event EventHandler<UsersGroupsEventArgs> ButtonRemoveUserFromGroupClick; 
        event EventHandler<RulesRolesEventArgs> ButtonRemoveRuleFromRoleClick;
        event EventHandler<RolesUsersEventArgs> ButtonRemoveRoleFromUserClick;
        event EventHandler<RolesGroupsEventArgs> ButtonRemoveRoleFromGroupClick;
        
         event EventHandler RefreshData;

        void SetUsers(ICollection<User> users);
        void SetUsersGroups(ICollection<UserGroup> usergroups); 
        void SetRoles(ICollection<Role> roles);
        void SetRules(ICollection<Rule> rules);

        event EventHandler<UserEventArgs> ButtonNewUserClick;
        event EventHandler<UserAndNameEventArgs> ButtonEditUserClick;

        event EventHandler<GroupEventArgs> ButtonNewGroupClick;
        event EventHandler<GroupAndNameEventArgs> ButtonEditUserGroupClick;

        event EventHandler<RoleEventArgs> ButtonNewRoleClick;
        event EventHandler<RoleAndNameEventArgs> ButtonEditRoleClick;

        event EventHandler<RuleEventArgs> ButtonNewRuleClick;
        event EventHandler<RuleAndNameEventArgs> ButtonEditRuleClick;
        
    }
}