﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Autentification;

namespace RuleRedactor
{
    public partial class EditRule : Form, IViewRules
    {
        public EditRule()
        {
            InitializeComponent();
        }

        RedactorForm main;

        private void EditRule_Load(object sender, EventArgs e)
        {
            main = this.Owner as RedactorForm;
            if (main != null && main.RuleForEditForm != null)
            {
                textBox1.Text = main.RuleForEditForm.Name;
                richTextBox1.Text = main.RuleForEditForm.Description;
            }
        }

        public event EventHandler<RuleEventArgs> ButtonNewRuleClick;

        public event EventHandler<RuleAndNameEventArgs> ButtonEditRuleClick;

        private void butOK_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Должно быть название права!!!");
                return;
            }
            if (main != null)
            {
                main.CreateOrEdit = true;
                main.ObjName = textBox1.Text;
                main.ObjDescription = richTextBox1.Text;

            }
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
