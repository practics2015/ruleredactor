﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification;

namespace RuleRedactor
{

    /// <summary>
    /// Класс для передачи пути к каталогу при загрузке или сохранение данных в файлы 
    /// </summary>
    public class PathEventArgs : EventArgs
    { 
        public string Path;

        public PathEventArgs(string path)
        {
            Path = path;
        }
    }

    /// <summary>
    /// Класс для события добавления нового пользователя
    /// </summary>
    public class UserEventArgs: EventArgs 
    { 
        public readonly User User;

        public UserEventArgs(User user)
        {
            User = user;
        }
            }

    /// <summary>
    /// Класс для события удаления списка пользователей
    /// </summary>
    public class UsersEventArgs: EventArgs 
    { 
        public ICollection<User> Users; 

        public UsersEventArgs(ICollection<User> users)
        {
            Users = users;
        }
    }       

    /// <summary>
    /// Класс для событий добавления в группы и удаления из них
    /// </summary>
    public class UsersGroupsEventArgs: EventArgs 
    { public ICollection<User> Users; 
      public ICollection<UserGroup> Groups;

      public UsersGroupsEventArgs(ICollection<User> users, ICollection<UserGroup> groups)
      {
          Users = users;
          Groups = groups;
      }
    }
    
    /// <summary>
    /// Класс для события добавления новой группы
    /// </summary>
    public class GroupEventArgs: EventArgs
    { 
        public UserGroup Group;

        public GroupEventArgs(UserGroup group)
        {
            Group = group;
        }
    }            

    /// <summary>
    /// Для события удаления групп пользователей
    /// </summary>
    public class GroupsEventArgs: EventArgs 
    { 
        public ICollection<UserGroup> Groups;

        public GroupsEventArgs(ICollection<UserGroup> groups)
        {
            Groups = groups;
        }
    }                             

    /// <summary>
    /// Класс для события добавления нового права
    /// </summary>
    public class RuleEventArgs: EventArgs 
    { 
        public Rule Rule;

        public RuleEventArgs(Rule rule)
        {
            Rule = rule;
        }
    }                                                       

    /// <summary>
    /// Класс для события удаления прав
    /// </summary>
    public class RulesEventArgs: EventArgs 
    { 
        public ICollection<Rule> Rules;

        public RulesEventArgs(ICollection<Rule> rules)
        {
            Rules = rules;
        }
    }                                     

    /// <summary>
    /// Класс для события добавления новой роли
    /// </summary>
    public class RoleEventArgs: EventArgs 
    { 
        public Role Role;

        public RoleEventArgs(Role role)
        {
            Role = role;
        }
    }                                                   

    /// <summary>
    /// Класс для события удаления ролей
    /// </summary>
    public class RolesEventArgs: EventArgs 
    { 
        public ICollection<Role> Roles;

        public RolesEventArgs(ICollection<Role> roles)
        {
            Roles = roles;
        }
    }                                      

    /// <summary>
    /// Для события добавления ролей группам и удаления из них
    /// </summary>
    public class RolesGroupsEventArgs: EventArgs
    { 
        public ICollection<Role> Roles;
        public ICollection<UserGroup> Groups;

        public RolesGroupsEventArgs(ICollection<Role> roles, ICollection<UserGroup> groups)
        {
            Roles = roles;
            Groups = groups;
        }
    }

    /// <summary>
    /// Класс для события добавления роли пользователям и удаления ролей пользователей
    /// </summary>
    public class RolesUsersEventArgs: EventArgs
    { 
        public ICollection<Role> Roles; 
        public ICollection<User> Users;

        public RolesUsersEventArgs(ICollection<Role> roles, ICollection<User> users)
        {
            Roles = roles;
            Users = users;
        }
    }                                                                     

    /// <summary>
    /// Класс для события добавления прав в роль и удаления прав из роли
    /// </summary>
    public class RulesRolesEventArgs: EventArgs
    { 
        public ICollection<Role> Roles;  
        public ICollection<Rule> Rules;

        public RulesRolesEventArgs(ICollection<Rule> rules, ICollection<Role> roles)
        {
            Roles = roles;
            Rules = rules;
        }
    
    }

    /// <summary>
    /// Класс для события изменения имени и описания пользователя
    /// </summary>
    public class UserAndNameEventArgs: EventArgs
    { 
        public User User; 
        public string NewSamName; 
        public string NewDisplayName;

        public UserAndNameEventArgs(User user, string newSamName, string newDisplayName)
        {
            User = user;
            NewDisplayName = newDisplayName;
            NewSamName = newSamName;
        }
    }
    
    /// <summary>
    /// Класс для события изменения названия группы
    /// </summary>
    public class GroupAndNameEventArgs: EventArgs
    { 
        public UserGroup UserGroup; 
        public string NewGroupName;

        public GroupAndNameEventArgs(UserGroup userGroup, string newGroupName)
        {
            UserGroup = userGroup;
            NewGroupName = newGroupName;
        }
    }
    
    /// <summary>
    /// Класс для события изменения названия и описания права
    /// </summary>
    public class RuleAndNameEventArgs: EventArgs
    { 
        public Rule Rule; 
        public string NewRuleName; 
        public string NewDescription;

        public RuleAndNameEventArgs(Rule rule, string newRuleName, string newDescription)
        {
            Rule = rule;
            NewRuleName = newRuleName;
            NewDescription = newDescription;
        }

    }
       
    /// <summary>
    /// Класс для события изменения названия роли
    /// </summary>
    public class RoleAndNameEventArgs: EventArgs
    { public Role Role; 
      public string NewRoleName;

      public RoleAndNameEventArgs(Role role, string newRoleName)
      {
          Role = role;
          NewRoleName = newRoleName;
      }

    }
    
}
