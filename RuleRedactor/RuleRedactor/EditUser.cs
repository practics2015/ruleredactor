﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Autentification;
using DevExpress.XtraTreeList;

namespace RuleRedactor
{
    public partial class EditUser : Form, IViewUsers
    {
        public EditUser()
        {
            InitializeComponent();
        }
        RedactorForm main;
        private void butOK_Click(object sender, EventArgs e)
        {
            OnButtonOKClick();
        }


        public event EventHandler<UserEventArgs> ButtonNewUserClick;

        public event EventHandler<UserAndNameEventArgs> ButtonEditUserClick;

        private void EditUser_Load(object sender, EventArgs e)
        {
            main = this.Owner as RedactorForm;
            if (main != null && main.UserForEditForm != null)
            {
                textBox1.Text = main.UserForEditForm.SamName;
                richTextBox1.Text = main.UserForEditForm.DisplayName;
            }
        }

        protected virtual void OnButtonOKClick()
        {
            //var handler = ButtonNewUserClick;
            if (textBox1.Text == "")
            {
                MessageBox.Show("Должно быть имя пользователя!!!");
                return;
            }
            if (main != null)
            {
                main.CreateOrEdit = true;
                main.ObjName = textBox1.Text;
                main.ObjDescription = richTextBox1.Text;

            }
            this.Close();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
