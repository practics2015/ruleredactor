﻿using System;
using System.Collections.Generic;
using Core;
using Core.Autentification;
using NLog;
using System.IO;

namespace RuleRedactor
{
    internal class UserManager : IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public CatalogUsers CatalogUsers 
        {
            get { return SettingsController<CatalogUsers>.Settings; }
        }
        public CatalogRules CatalogRules
        {
            get { return SettingsController<CatalogRules>.Settings; }
        }

        public CatalogRoles CatalogRoles
        {
            get { return SettingsController<CatalogRoles>.Settings; }
        }

        public CatalogUserGroups CatalogUserGroups
        {
            get { return SettingsController<CatalogUserGroups>.Settings; }
        }


        public UserManager()
        {

            
            
        }

        public ICollection<Rule> GetRulesByUserName(string samUserName)
        {
            // выбираем права пользователя из прав самого пользователя
            var user = CatalogUsers.GetUser(samUserName);
            return user.GetUserRules();
        }

        public void LoadCatalogs()
        {
            SettingsController<CatalogUsers>.LoadSettings();
            SettingsController<CatalogRules>.LoadSettings();
            SettingsController<CatalogRoles>.LoadSettings();
            SettingsController<CatalogUserGroups>.LoadSettings();
        }

        public void LoadCatalogs(string path)
        {
            SettingsController<CatalogUsers>.LoadSettings(Path.Combine(path, "Core.Autentification.CatalogUsers.xml"));
            SettingsController<CatalogRules>.LoadSettings(Path.Combine(path, "Core.Autentification.CatalogRules.xml"));
            SettingsController<CatalogRoles>.LoadSettings(Path.Combine(path, "Core.Autentification.CatalogRoles.xml"));
            SettingsController<CatalogUserGroups>.LoadSettings(Path.Combine(path, "Core.Autentification.CatalogUserGroups.xml"));
        }

        public void SaveCatalogs()
        {
            //// сохраняем тестовго узера
            //CatalogRules.AddRule(new RuleService() { ServiceName = "DbManager.WellsLogic.UploadWellFromWitsml" });
            //CatalogRules.AddRule(new RuleService() { ServiceName = "Esb.RegisterModule" });
            //CatalogRules.AddRule(new RuleService() { ServiceName = "Esb.RegisterServices" });
            //CatalogRules.AddRule(new RuleService() { ServiceName = "ServerApplication.System.RegisterServices" });
            //CatalogRules.AddRule(new RuleService() { ServiceName = "ServerApplication.System.UserAutentificationService" });
            //CatalogRules.AddRule(new RuleService() { ServiceName = "ServerApplication.LoadWellFromTxtHeader" });

            //var allRole = new Role() { Name = "All", Rules = CatalogRules.GetAllRules() };
            //CatalogRoles.AddRole(allRole);

            //var userNipi_049 = new User() { SamName = "Nipi_049", DisplayName = "Епишин Константин" };
            //userNipi_049.Roles.Add(allRole);
            //CatalogUsers.AddUser(userNipi_049);
            ////

            SettingsController<CatalogUsers>.SaveSettings();
            SettingsController<CatalogRules>.SaveSettings();
            SettingsController<CatalogRoles>.SaveSettings();
            SettingsController<CatalogUserGroups>.SaveSettings();
            
        }

        public void SaveCatalogs(string path)
        {
            SettingsController<CatalogUsers>.SaveSettings(Path.Combine(path, "Core.Autentification.CatalogUsers.xml"));
            SettingsController<CatalogRules>.SaveSettings(Path.Combine(path, "Core.Autentification.CatalogRules.xml"));
            SettingsController<CatalogRoles>.SaveSettings(Path.Combine(path, "Core.Autentification.CatalogRoles.xml"));
            SettingsController<CatalogUserGroups>.SaveSettings(Path.Combine(path, "Core.Autentification.CatalogUserGroups.xml"));

        }

        public void Dispose()
        {
            SaveCatalogs();
        }

        public User GetRegisteredUser(string samUserName)
        {
            return CatalogUsers.GetUser(samUserName);
        }

        public User GetRegisteredUserByUserId(string userId)
        {
            return CatalogUsers.GetUserByUserId(userId);
        }

    
    }
}