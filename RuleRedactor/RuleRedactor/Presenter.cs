﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification;



namespace RuleRedactor
{
    class Presenter : IDisposable
    {
        
        private UserManager _model;
        private IView _view;

        public Presenter(UserManager model, IView view)
        {
            _model = model;
            _view = view;

            SignViewEvents();
        }

        private void SignViewEvents()
        {
            _view.ButtonLoadClick += _view_ButtonLoadClick;
            _view.ButtonSaveClick+=_view_ButtonSaveClick;

            //Пользователи     
            _view.ButtonDeleteUserClick += _view_ButtonDeleteUserClick;            
            _view.ButtonAddUserToGroupClick += _view_ButtonAddUserToGroupClick;            
            _view.ButtonRemoveUserFromGroupClick += _view_ButtonRemoveUserFromGroupClick;

            //Группы пользователей
            _view.ButtonDeleteGroupClick += _view_ButtonDeleteGroupClick;

            //Права      
            _view.ButtonDeleteRuleClick += _view_ButtonDeleteRuleClick;
            _view.ButtonAddRuleToRoleClick += _view_ButtonAddRuleToRoleClick;
            _view.ButtonRemoveRuleFromRoleClick += _view_ButtonRemoveRuleFromRoleClick;

            //Роли
            _view.ButtonDeleteRoleClick += _view_ButtonDeleteRoleClick;
            _view.ButtonAddRoleToGroupClick += _view_ButtonAddRoleToGroupClick;           
            _view.ButtonAddRoleToUserClick += _view_ButtonAddRoleToUserClick;
            _view.ButtonRemoveRoleFromGroupClick += _view_ButtonRemoveRoleFromGroupClick;
            _view.ButtonRemoveRoleFromUserClick += _view_ButtonRemoveRoleFromUserClick;
            _view.RefreshData += _view_RefreshData;



            _view.ButtonNewUserClick += _view_ButtonNewUserClick;
            _view.ButtonEditUserClick += _view_ButtonCorrectUserClick;

            _view.ButtonNewGroupClick += _view_ButtonNewGroupClick;
            _view.ButtonEditUserGroupClick += _view_ButtonCorrectUserGroupClick;

            _view.ButtonNewRoleClick += _view_ButtonNewRoleClick;
            _view.ButtonEditRoleClick += _view_ButtonCorrectRoleClick;

            _view.ButtonNewRuleClick += _view_ButtonNewRuleClick;
            _view.ButtonEditRuleClick += _view_ButtonCorrectRuleClick;
            
        }

        void _view_RefreshData(object sender, EventArgs e)
        {
            _view.SetUsers(_model.CatalogUsers.Users);
            _view.SetUsersGroups(_model.CatalogUserGroups.Groups);
            _view.SetRoles(_model.CatalogRoles.Roles);
            _view.SetRules(_model.CatalogRules.Rules);
        }


        //Методы для загрузки и сохранения данных в файл

        /// <summary>
        /// Загружает каталоги с данныи о пользователях, группах, правах и ролях из указанного каталога
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonLoadClick(object sender, PathEventArgs e)
        {
            if (e.Path != "")
                _model.LoadCatalogs(e.Path);
            else
                _model.LoadCatalogs();

            _view.SetUsers(_model.CatalogUsers.Users);                // вызваем вывод пользователей
            _view.SetUsersGroups(_model.CatalogUserGroups.Groups);     //групп пользователей и т.д.
            _view.SetRoles(_model.CatalogRoles.Roles);
            _view.SetRules(_model.CatalogRules.Rules);
        }

        /// <summary>
        /// Сохраняет файлы с данными в указаный каталог (имена файлов зарезервированы)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonSaveClick(object sender, PathEventArgs e)
        {
            if (e.Path != "")
                _model.SaveCatalogs(e.Path);
            else
                _model.SaveCatalogs();
        }

 
        //Методы для добавления объектов в списки

        /// <summary>
        /// Добавление списка пользователей в группы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonAddUserToGroupClick(object sender, UsersGroupsEventArgs e)
        {
            foreach (var group in e.Groups)
            {
                foreach (var user in e.Users)
                {
                    user.AddUserGroup(group);
                    group.AddUser(user);
                }
            }
        }

        /// <summary>
        /// Добавляет права в список ролей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonAddRuleToRoleClick(object sender, RulesRolesEventArgs e)
        {
            foreach (var role in e.Roles)
            {
                foreach (var rule in e.Rules)
                {
                    role.AddRule(rule);
                }
            }
        }

        /// <summary>
        /// Присвоение ролей пользователям
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonAddRoleToUserClick(object sender, RolesUsersEventArgs e)
        {
            foreach (var user in e.Users)
            {
                foreach (var role in e.Roles)
                    user.AddRole(role);
            }
        }

        /// <summary>
        /// Добавляет роли списку групп пользователей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonAddRoleToGroupClick(object sender, RolesGroupsEventArgs e)
        {
            foreach (var group in e.Groups)
            {
                foreach (var role in e.Roles)
                {
                    group.AddRole(role);
                }
            }
        }


        //Методы для удаления объектов из списков

        /// <summary>
        /// Удаление списка пользователей из групп
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonRemoveUserFromGroupClick(object sender, UsersGroupsEventArgs e)
        {
            foreach (var group in e.Groups)
            {
                foreach (var user in e.Users)
                {
                    group.RemoveUser(user);
                    user.RemoveUserGroup(group);
                }
            }
        }

        /// <summary>
        /// Удаление списка ролей у списка пользователей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonRemoveRoleFromUserClick(object sender, RolesUsersEventArgs e)
        {
            foreach (var user in e.Users)
            {
                foreach (var role in e.Roles)
                {
                    user.RemoveRole(role);
                }
            }
        }

        /// <summary>
        /// Удаление списка ролей у списка групп пользователей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonRemoveRoleFromGroupClick(object sender, RolesGroupsEventArgs e)
        {
            foreach (var group in e.Groups)
            {
                foreach (var role in e.Roles)
                {
                    group.RemoveRole(role);
                }
            }
        }

        /// <summary>
        /// Удаляет список прав из списка ролей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonRemoveRuleFromRoleClick(object sender, RulesRolesEventArgs e)
        {
            foreach (var role in e.Roles)
            {
                foreach(var rule in e.Rules)
                {
                    role.RemoveRule(rule);
                }
            }
        }

        //Методы для удаления объектов 
       
        /// <summary>
        /// Удаляет список ролей из каталога ролей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonDeleteRoleClick(object sender, RolesEventArgs e)
        {
            foreach (var role in e.Roles)
            {
                _model.CatalogRoles.RemoveRole(role);
                _model.CatalogUserGroups.RemoveRole(role);
                _model.CatalogUsers.RemoveRole(role);
            }
        }

        /// <summary>
        /// Удаляет список прав из каталога прав
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonDeleteRuleClick(object sender, RulesEventArgs e)
        {
            foreach (var rule in e.Rules)
            {
                _model.CatalogRules.RemoveRule(rule);

                _model.CatalogRoles.RemoveRule(rule);
            }
        }
          
        /// <summary>
        /// Удаление группы пользователей из каталога групп
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonDeleteGroupClick(object sender, GroupsEventArgs e)
        {
            foreach(var group in e.Groups)
            {
                _model.CatalogUserGroups.RemoveUserGroup(group);
                _model.CatalogUsers.RemoveUserGroup(group);
            }
        }

        /// <summary>
        /// Удаление пользователей из каталога пользователей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonDeleteUserClick(object sender, UsersEventArgs e)
        {
            foreach (var user in e.Users)
            {
                foreach (var group in _model.CatalogUserGroups.Groups)
                {
                    group.RemoveUser(user); 
                }
                _model.CatalogUsers.RemoveUser(user); 
            }
        }
       


        //////////////

        /// <summary>
        /// Добавление нового пользователя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewUserClick(object sender, UserEventArgs e)
        {
            e.User.Id = User.GenerateUserId();
            _model.CatalogUsers.AddUser(e.User);
        }

        /// <summary>
        /// Изменяет логин пользователя и его описание на новые
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectUserClick(object sender, UserAndNameEventArgs e)
        {
            _model.CatalogUsers.ChangeUserNames(e.User, e.NewSamName, e.NewDisplayName);

        }

        /// <summary>
        /// Добавление новоей группы пользователей в каталог групп
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewGroupClick(object sender, GroupEventArgs e)
        {
            _model.CatalogUserGroups.AddUserGroup(e.Group);
        }

        /// <summary>
        /// Изменяет название группы пользователей на новое
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectUserGroupClick(object sender, GroupAndNameEventArgs e)
        {
            _model.CatalogUserGroups.ChangeGroupName(e.UserGroup, e.NewGroupName);
        }

        /// <summary>
        /// Добавляет новую роль в каталог ролей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewRoleClick(object sender, RoleEventArgs e)
        {
            _model.CatalogRoles.AddRole(e.Role);
        }


        /// <summary>
        /// Меняет название роли на новое
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectRoleClick(object sender, RoleAndNameEventArgs e)
        {
            _model.CatalogRoles.ChangeRoleName(e.Role, e.NewRoleName);

        }

        /// <summary>
        /// Добавление нового права в каталог прав
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewRuleClick(object sender, RuleEventArgs e)
        {
            _model.CatalogRules.AddRule(e.Rule);
        }

        /// <summary>
        /// Меняет название права и его описание на новое
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectRuleClick(object sender, RuleAndNameEventArgs e)
        {
            _model.CatalogRules.ChangeRule(e.Rule, e.NewRuleName, e.NewDescription);
        }

        private void UnSignViewEvents()
        {
            _view.ButtonLoadClick -= _view_ButtonLoadClick;
            _view.ButtonSaveClick -= _view_ButtonSaveClick;

            //Пользователи     
            _view.ButtonDeleteUserClick -= _view_ButtonDeleteUserClick;
            _view.ButtonAddUserToGroupClick -= _view_ButtonAddUserToGroupClick;
            _view.ButtonRemoveUserFromGroupClick -= _view_ButtonRemoveUserFromGroupClick;

            //Группы пользователей
            _view.ButtonDeleteGroupClick -= _view_ButtonDeleteGroupClick;

            //Права      
            _view.ButtonDeleteRuleClick -= _view_ButtonDeleteRuleClick;
            _view.ButtonAddRuleToRoleClick -= _view_ButtonAddRuleToRoleClick;
            _view.ButtonRemoveRuleFromRoleClick -= _view_ButtonRemoveRuleFromRoleClick;

            //Роли
            _view.ButtonDeleteRoleClick -= _view_ButtonDeleteRoleClick;
            _view.ButtonAddRoleToGroupClick -= _view_ButtonAddRoleToGroupClick;
            _view.ButtonAddRoleToUserClick -= _view_ButtonAddRoleToUserClick;
            _view.ButtonRemoveRoleFromGroupClick -= _view_ButtonRemoveRoleFromGroupClick;
            _view.ButtonRemoveRoleFromUserClick -= _view_ButtonRemoveRoleFromUserClick;


            _view.ButtonNewUserClick -= _view_ButtonNewUserClick;
            _view.ButtonEditUserClick -= _view_ButtonCorrectUserClick;

            _view.ButtonNewGroupClick -= _view_ButtonNewGroupClick;
            _view.ButtonEditUserGroupClick -= _view_ButtonCorrectUserGroupClick;

            _view.ButtonNewRoleClick -= _view_ButtonNewRoleClick;
            _view.ButtonEditRoleClick -= _view_ButtonCorrectRoleClick;

            _view.ButtonNewRuleClick -= _view_ButtonNewRuleClick;
            _view.ButtonEditRuleClick -= _view_ButtonCorrectRuleClick;
        }

        public void Dispose()
        {
            UnSignViewEvents();
        }

    }
}
