﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleRedactor
{
    interface IViewUsers
    {
        event EventHandler<UserEventArgs> ButtonNewUserClick;
        event EventHandler<UserAndNameEventArgs> ButtonEditUserClick;
    }
}
