﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleRedactor
{
    interface IViewRules
    {
        event EventHandler<RuleEventArgs> ButtonNewRuleClick;      // событие добавления нового права
        event EventHandler<RuleAndNameEventArgs> ButtonEditRuleClick;
    }
}
