﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleRedactor
{
    interface IViewGroups
    {
        event EventHandler<GroupEventArgs> ButtonNewGroupClick;         // событие добавления новой группы пользователей
        event EventHandler<GroupAndNameEventArgs> ButtonEditUserGroupClick;
    }
}
