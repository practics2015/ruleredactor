﻿namespace RuleRedactor
{
    partial class RedactorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RedactorForm));
            this.tlUsers = new DevExpress.XtraTreeList.TreeList();
            this.tlcUserSamName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlcUserDisplayName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlGroups = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn12 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlRoles = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn13 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.tlRules = new DevExpress.XtraTreeList.TreeList();
            this.treeListColumn14 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.treeListColumn15 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.butNewUser = new System.Windows.Forms.Button();
            this.butDeleteUser = new System.Windows.Forms.Button();
            this.butNewGroup = new System.Windows.Forms.Button();
            this.butNewRole = new System.Windows.Forms.Button();
            this.butNewRule = new System.Windows.Forms.Button();
            this.butDeleteRule = new System.Windows.Forms.Button();
            this.butDeleteRole = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.butAddUserToGroup = new System.Windows.Forms.Button();
            this.butAddRoleToGroup = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.butAddRuleToRole = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.butDeleteUserFromGroup = new System.Windows.Forms.Button();
            this.butEditUser = new System.Windows.Forms.Button();
            this.butEditRule = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.butEditGroup = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.tlUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlRules)).BeginInit();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlUsers
            // 
            this.tlUsers.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.tlcUserSamName,
            this.tlcUserDisplayName});
            this.tlUsers.Location = new System.Drawing.Point(12, 93);
            this.tlUsers.Name = "tlUsers";
            this.tlUsers.Size = new System.Drawing.Size(221, 400);
            this.tlUsers.TabIndex = 0;
            this.tlUsers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tlUsers_KeyDown);
            // 
            // tlcUserSamName
            // 
            this.tlcUserSamName.Caption = "Имя";
            this.tlcUserSamName.FieldName = "treeListColumn10";
            this.tlcUserSamName.Name = "tlcUserSamName";
            this.tlcUserSamName.Visible = true;
            this.tlcUserSamName.VisibleIndex = 0;
            // 
            // tlcUserDisplayName
            // 
            this.tlcUserDisplayName.Caption = "Описание";
            this.tlcUserDisplayName.FieldName = "treeListColumn11";
            this.tlcUserDisplayName.Name = "tlcUserDisplayName";
            this.tlcUserDisplayName.Visible = true;
            this.tlcUserDisplayName.VisibleIndex = 1;
            // 
            // tlGroups
            // 
            this.tlGroups.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn12});
            this.tlGroups.Location = new System.Drawing.Point(247, 93);
            this.tlGroups.Name = "tlGroups";
            this.tlGroups.Size = new System.Drawing.Size(184, 400);
            this.tlGroups.TabIndex = 1;
            this.tlGroups.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tlGroups_KeyDown);
            // 
            // treeListColumn12
            // 
            this.treeListColumn12.Caption = "Название";
            this.treeListColumn12.FieldName = "treeListColumn12";
            this.treeListColumn12.Name = "treeListColumn12";
            this.treeListColumn12.Visible = true;
            this.treeListColumn12.VisibleIndex = 0;
            // 
            // tlRoles
            // 
            this.tlRoles.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn13});
            this.tlRoles.Location = new System.Drawing.Point(446, 93);
            this.tlRoles.Name = "tlRoles";
            this.tlRoles.Size = new System.Drawing.Size(184, 400);
            this.tlRoles.TabIndex = 2;
            this.tlRoles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tlRoles_KeyDown);
            // 
            // treeListColumn13
            // 
            this.treeListColumn13.Caption = "Название";
            this.treeListColumn13.FieldName = "treeListColumn13";
            this.treeListColumn13.Name = "treeListColumn13";
            this.treeListColumn13.Visible = true;
            this.treeListColumn13.VisibleIndex = 0;
            // 
            // tlRules
            // 
            this.tlRules.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.treeListColumn14,
            this.treeListColumn15});
            this.tlRules.Location = new System.Drawing.Point(645, 93);
            this.tlRules.Name = "tlRules";
            this.tlRules.Size = new System.Drawing.Size(184, 400);
            this.tlRules.TabIndex = 3;
            this.tlRules.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tlRules_KeyDown);
            // 
            // treeListColumn14
            // 
            this.treeListColumn14.Caption = "Название";
            this.treeListColumn14.FieldName = "treeListColumn14";
            this.treeListColumn14.Name = "treeListColumn14";
            this.treeListColumn14.OptionsColumn.AllowEdit = false;
            this.treeListColumn14.Visible = true;
            this.treeListColumn14.VisibleIndex = 0;
            // 
            // treeListColumn15
            // 
            this.treeListColumn15.Caption = "Описание";
            this.treeListColumn15.FieldName = "Описание";
            this.treeListColumn15.Name = "treeListColumn15";
            this.treeListColumn15.Visible = true;
            this.treeListColumn15.VisibleIndex = 1;
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(843, 24);
            this.menuStrip2.TabIndex = 4;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(82, 20);
            this.toolStripMenuItem1.Text = "Открыть";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(93, 20);
            this.toolStripMenuItem2.Text = "Сохранить";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // butNewUser
            // 
            this.butNewUser.ImageKey = "(none)";
            this.butNewUser.Location = new System.Drawing.Point(12, 36);
            this.butNewUser.Name = "butNewUser";
            this.butNewUser.Size = new System.Drawing.Size(34, 34);
            this.butNewUser.TabIndex = 5;
            this.butNewUser.UseVisualStyleBackColor = true;
            this.butNewUser.Click += new System.EventHandler(this.butNewUser_Click);
            // 
            // butDeleteUser
            // 
            this.butDeleteUser.Image = ((System.Drawing.Image)(resources.GetObject("butDeleteUser.Image")));
            this.butDeleteUser.Location = new System.Drawing.Point(52, 36);
            this.butDeleteUser.Name = "butDeleteUser";
            this.butDeleteUser.Size = new System.Drawing.Size(34, 34);
            this.butDeleteUser.TabIndex = 6;
            this.butDeleteUser.UseVisualStyleBackColor = true;
            this.butDeleteUser.Click += new System.EventHandler(this.butDeleteUser_Click);
            // 
            // butNewGroup
            // 
            this.butNewGroup.Image = ((System.Drawing.Image)(resources.GetObject("butNewGroup.Image")));
            this.butNewGroup.Location = new System.Drawing.Point(247, 36);
            this.butNewGroup.Name = "butNewGroup";
            this.butNewGroup.Size = new System.Drawing.Size(34, 34);
            this.butNewGroup.TabIndex = 7;
            this.butNewGroup.UseVisualStyleBackColor = true;
            this.butNewGroup.Click += new System.EventHandler(this.butNewGroup_Click);
            // 
            // butNewRole
            // 
            this.butNewRole.Image = ((System.Drawing.Image)(resources.GetObject("butNewRole.Image")));
            this.butNewRole.Location = new System.Drawing.Point(446, 36);
            this.butNewRole.Name = "butNewRole";
            this.butNewRole.Size = new System.Drawing.Size(34, 34);
            this.butNewRole.TabIndex = 8;
            this.butNewRole.UseVisualStyleBackColor = true;
            this.butNewRole.Click += new System.EventHandler(this.butNewRole_Click);
            // 
            // butNewRule
            // 
            this.butNewRule.Image = ((System.Drawing.Image)(resources.GetObject("butNewRule.Image")));
            this.butNewRule.Location = new System.Drawing.Point(644, 36);
            this.butNewRule.Name = "butNewRule";
            this.butNewRule.Size = new System.Drawing.Size(34, 34);
            this.butNewRule.TabIndex = 9;
            this.butNewRule.UseVisualStyleBackColor = true;
            this.butNewRule.Click += new System.EventHandler(this.butNewRule_Click);
            // 
            // butDeleteRule
            // 
            this.butDeleteRule.Image = ((System.Drawing.Image)(resources.GetObject("butDeleteRule.Image")));
            this.butDeleteRule.Location = new System.Drawing.Point(684, 36);
            this.butDeleteRule.Name = "butDeleteRule";
            this.butDeleteRule.Size = new System.Drawing.Size(34, 34);
            this.butDeleteRule.TabIndex = 10;
            this.butDeleteRule.UseVisualStyleBackColor = true;
            this.butDeleteRule.Click += new System.EventHandler(this.butDeleteRule_Click);
            // 
            // butDeleteRole
            // 
            this.butDeleteRole.Image = ((System.Drawing.Image)(resources.GetObject("butDeleteRole.Image")));
            this.butDeleteRole.Location = new System.Drawing.Point(486, 36);
            this.butDeleteRole.Name = "butDeleteRole";
            this.butDeleteRole.Size = new System.Drawing.Size(34, 34);
            this.butDeleteRole.TabIndex = 11;
            this.butDeleteRole.UseVisualStyleBackColor = true;
            this.butDeleteRole.Click += new System.EventHandler(this.butDeleteRole_Click);
            // 
            // button10
            // 
            this.button10.Image = ((System.Drawing.Image)(resources.GetObject("button10.Image")));
            this.button10.Location = new System.Drawing.Point(290, 36);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(34, 34);
            this.button10.TabIndex = 12;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.butDeleteGroup_Click);
            // 
            // butAddUserToGroup
            // 
            this.butAddUserToGroup.Image = ((System.Drawing.Image)(resources.GetObject("butAddUserToGroup.Image")));
            this.butAddUserToGroup.Location = new System.Drawing.Point(92, 36);
            this.butAddUserToGroup.Name = "butAddUserToGroup";
            this.butAddUserToGroup.Size = new System.Drawing.Size(34, 34);
            this.butAddUserToGroup.TabIndex = 13;
            this.butAddUserToGroup.Text = "              Добавить пользователя в группу";
            this.butAddUserToGroup.UseVisualStyleBackColor = true;
            this.butAddUserToGroup.Click += new System.EventHandler(this.butAddUserToGroup_Click);
            // 
            // butAddRoleToGroup
            // 
            this.butAddRoleToGroup.Image = ((System.Drawing.Image)(resources.GetObject("butAddRoleToGroup.Image")));
            this.butAddRoleToGroup.Location = new System.Drawing.Point(526, 36);
            this.butAddRoleToGroup.Name = "butAddRoleToGroup";
            this.butAddRoleToGroup.Size = new System.Drawing.Size(34, 34);
            this.butAddRoleToGroup.TabIndex = 14;
            this.butAddRoleToGroup.Text = "               Добавить роль группе  ";
            this.butAddRoleToGroup.UseVisualStyleBackColor = true;
            this.butAddRoleToGroup.Click += new System.EventHandler(this.butAddRoleToGroup_Click);
            // 
            // button13
            // 
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.Location = new System.Drawing.Point(330, 36);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(34, 34);
            this.button13.TabIndex = 15;
            this.button13.Text = "              Добавить права в роль";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // butAddRuleToRole
            // 
            this.butAddRuleToRole.Image = ((System.Drawing.Image)(resources.GetObject("butAddRuleToRole.Image")));
            this.butAddRuleToRole.Location = new System.Drawing.Point(724, 36);
            this.butAddRuleToRole.Name = "butAddRuleToRole";
            this.butAddRuleToRole.Size = new System.Drawing.Size(34, 34);
            this.butAddRuleToRole.TabIndex = 16;
            this.butAddRuleToRole.Text = "              Добавить роль пользователю";
            this.butAddRuleToRole.UseVisualStyleBackColor = true;
            this.butAddRuleToRole.Click += new System.EventHandler(this.butAddRuleToRole_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Пользователи";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(642, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Права";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(443, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Роли";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(244, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Группы";
            // 
            // butDeleteUserFromGroup
            // 
            this.butDeleteUserFromGroup.Image = ((System.Drawing.Image)(resources.GetObject("butDeleteUserFromGroup.Image")));
            this.butDeleteUserFromGroup.Location = new System.Drawing.Point(132, 36);
            this.butDeleteUserFromGroup.Name = "butDeleteUserFromGroup";
            this.butDeleteUserFromGroup.Size = new System.Drawing.Size(34, 34);
            this.butDeleteUserFromGroup.TabIndex = 21;
            this.butDeleteUserFromGroup.Text = "              Удалить пользователя из группы";
            this.butDeleteUserFromGroup.UseVisualStyleBackColor = true;
            this.butDeleteUserFromGroup.Click += new System.EventHandler(this.butDeleteUserFromGroup_Click);
            // 
            // butEditUser
            // 
            this.butEditUser.Image = ((System.Drawing.Image)(resources.GetObject("butEditUser.Image")));
            this.butEditUser.Location = new System.Drawing.Point(172, 36);
            this.butEditUser.Name = "butEditUser";
            this.butEditUser.Size = new System.Drawing.Size(34, 34);
            this.butEditUser.TabIndex = 22;
            this.butEditUser.Text = "                 Редактировать";
            this.butEditUser.UseVisualStyleBackColor = true;
            this.butEditUser.Click += new System.EventHandler(this.butEditUser_Click);
            // 
            // butEditRule
            // 
            this.butEditRule.Image = ((System.Drawing.Image)(resources.GetObject("butEditRule.Image")));
            this.butEditRule.Location = new System.Drawing.Point(764, 36);
            this.butEditRule.Name = "butEditRule";
            this.butEditRule.Size = new System.Drawing.Size(34, 34);
            this.butEditRule.TabIndex = 23;
            this.butEditRule.Text = "                 Редактировать";
            this.butEditRule.UseVisualStyleBackColor = true;
            this.butEditRule.Click += new System.EventHandler(this.butEditRule_Click);
            // 
            // button18
            // 
            this.button18.Image = ((System.Drawing.Image)(resources.GetObject("button18.Image")));
            this.button18.Location = new System.Drawing.Point(566, 36);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(34, 34);
            this.button18.TabIndex = 24;
            this.button18.Text = "                 Редактировать";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.butEditRole_Click);
            // 
            // butEditGroup
            // 
            this.butEditGroup.Image = ((System.Drawing.Image)(resources.GetObject("butEditGroup.Image")));
            this.butEditGroup.Location = new System.Drawing.Point(370, 36);
            this.butEditGroup.Name = "butEditGroup";
            this.butEditGroup.Size = new System.Drawing.Size(34, 34);
            this.butEditGroup.TabIndex = 25;
            this.butEditGroup.Text = "                 Редактировать";
            this.butEditGroup.UseVisualStyleBackColor = true;
            this.butEditGroup.Click += new System.EventHandler(this.butEditGroup_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // RedactorForm
            // 
            this.ClientSize = new System.Drawing.Size(843, 519);
            this.Controls.Add(this.butEditGroup);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.butEditRule);
            this.Controls.Add(this.butEditUser);
            this.Controls.Add(this.butDeleteUserFromGroup);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.butAddRuleToRole);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.butAddRoleToGroup);
            this.Controls.Add(this.butAddUserToGroup);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.butDeleteRole);
            this.Controls.Add(this.butDeleteRule);
            this.Controls.Add(this.butNewRule);
            this.Controls.Add(this.butNewRole);
            this.Controls.Add(this.butNewGroup);
            this.Controls.Add(this.butDeleteUser);
            this.Controls.Add(this.butNewUser);
            this.Controls.Add(this.tlRules);
            this.Controls.Add(this.tlRoles);
            this.Controls.Add(this.tlGroups);
            this.Controls.Add(this.tlUsers);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip2;
            this.MaximumSize = new System.Drawing.Size(859, 557);
            this.MinimumSize = new System.Drawing.Size(859, 557);
            this.Name = "RedactorForm";
            this.Text = "RuleRedactor";
            this.Load += new System.EventHandler(this.RedactorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tlUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlRules)).EndInit();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn4;
        private DevExpress.XtraTreeList.TreeList treeList5;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn6;
        private DevExpress.XtraTreeList.TreeList treeList4;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn8;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn3;
        private DevExpress.XtraTreeList.TreeList treeList2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn7;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn5;
        private DevExpress.XtraTreeList.TreeList treeList3;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn9;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcUserSamName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn tlcUserDisplayName;
        private DevExpress.XtraTreeList.TreeList tlGroups;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn12;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn13;
        private DevExpress.XtraTreeList.TreeList tlRules;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn14;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Button butNewUser;
        private System.Windows.Forms.Button butDeleteUser;
        private System.Windows.Forms.Button butNewGroup;
        private System.Windows.Forms.Button butNewRole;
        private System.Windows.Forms.Button butNewRule;
        private System.Windows.Forms.Button butDeleteRule;
        private System.Windows.Forms.Button butDeleteRole;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button butAddUserToGroup;
        private System.Windows.Forms.Button butAddRoleToGroup;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button butAddRuleToRole;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button butDeleteUserFromGroup;
        private System.Windows.Forms.Button butEditUser;
        private System.Windows.Forms.Button butEditRule;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button butEditGroup;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevExpress.XtraTreeList.Columns.TreeListColumn treeListColumn15;
        protected DevExpress.XtraTreeList.TreeList tlRoles;
        private DevExpress.XtraTreeList.TreeList tlUsers;
    }
}

