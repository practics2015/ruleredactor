﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification;

namespace RuleRedactor
{
    class PresenterUsers: IDisposable
    {
        private UserManager _model;
        private IViewUsers _view;

        public PresenterUsers(UserManager model, IViewUsers view)
        {
            _model = model;
            _view = view;

            SignViewEvents();
        }

        private void SignViewEvents()
        {
           
            _view.ButtonNewUserClick += _view_ButtonNewUserClick;
            _view.ButtonEditUserClick += _view_ButtonCorrectUserClick;

        }

        /// <summary>
        /// Добавление нового пользователя
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewUserClick(object sender, UserEventArgs e)
        {
            e.User.Id = User.GenerateUserId();
            _model.CatalogUsers.AddUser(e.User);
        }

        /// <summary>
        /// Изменяет логин пользователя и его описание на новые
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectUserClick(object sender, UserAndNameEventArgs e)
        {
            _model.CatalogUsers.ChangeUserNames(e.User, e.NewSamName, e.NewDisplayName);

        }

        private void UnSignViewEvents()
        {
            _view.ButtonNewUserClick -= _view_ButtonNewUserClick;
            _view.ButtonEditUserClick -= _view_ButtonCorrectUserClick;
        }

        public void Dispose()
        {
            UnSignViewEvents();
        }
    }
}
