﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification;

namespace RuleRedactor
{
    class PresenterRules: IDisposable
    {
        private UserManager _model;
        private IViewRules _view;
        public PresenterRules(UserManager model, IViewRules view)
        {
            _model = model;
            _view = view;

            SignViewEvents();
        }

        private void SignViewEvents()
        {
            _view.ButtonNewRuleClick += _view_ButtonNewRuleClick;
            _view.ButtonEditRuleClick += _view_ButtonCorrectRuleClick;
        }

        /// <summary>
        /// Добавление нового права в каталог прав
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewRuleClick(object sender, RuleEventArgs e)
        {
            _model.CatalogRules.AddRule(e.Rule);
        }

        /// <summary>
        /// Меняет название права и его описание на новое
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectRuleClick(object sender, RuleAndNameEventArgs e)
        {
            _model.CatalogRules.ChangeRule(e.Rule, e.NewRuleName, e.NewDescription);
        }

        private void UnSignViewEvents()
        {
            _view.ButtonNewRuleClick -= _view_ButtonNewRuleClick;
            _view.ButtonEditRuleClick -= _view_ButtonCorrectRuleClick;
        }

        public void Dispose()
        {
            UnSignViewEvents();
        }
    }
}
