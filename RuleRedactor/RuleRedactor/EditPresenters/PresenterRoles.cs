﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification; //добавил Смагин 02.07



namespace RuleRedactor
{
    class PresenterRoles : IDisposable
    {

        private UserManager _model;
        private IViewRoles _view;

        public PresenterRoles(UserManager model, IViewRoles view)
        {
            _model = model;
            _view = view;

            SignViewEvents();
        }

        private void SignViewEvents()
        {
            _view.ButtonNewRoleClick += _view_ButtonNewRoleClick;
            _view.ButtonEditRoleClick += _view_ButtonCorrectRoleClick;
        }

        /// <summary>
        /// Добавляет новую роль в каталог ролей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewRoleClick(object sender, RoleEventArgs e)
        {
            _model.CatalogRoles.AddRole(e.Role);
        }


        /// <summary>
        /// Меняет название роли на новое
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectRoleClick(object sender, RoleAndNameEventArgs e)
        {
            _model.CatalogRoles.ChangeRoleName(e.Role, e.NewRoleName);

        }

        private void UnSignViewEvents()
        {
            _view.ButtonNewRoleClick -= _view_ButtonNewRoleClick;
            _view.ButtonEditRoleClick -= _view_ButtonCorrectRoleClick;
        }

        public void Dispose()
        {
            UnSignViewEvents();
        }
    
    }


}