﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Autentification;

namespace RuleRedactor
{
    class PresenterGroups: IDisposable
    {
         private UserManager _model;
        private IViewGroups _view;

        public PresenterGroups(UserManager model, IViewGroups view)
        {
            _model = model;
            _view = view;

            SignViewEvents();
        }

        private void SignViewEvents()
        {
            _view.ButtonNewGroupClick += _view_ButtonNewGroupClick;           
            _view.ButtonEditUserGroupClick += _view_ButtonCorrectUserGroupClick;
        }

        /// <summary>
        /// Добавление новоей группы пользователей в каталог групп
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonNewGroupClick(object sender, GroupEventArgs e)
        {
            _model.CatalogUserGroups.AddUserGroup(e.Group);
        }

        /// <summary>
        /// Изменяет название группы пользователей на новое
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _view_ButtonCorrectUserGroupClick(object sender, GroupAndNameEventArgs e)
        {
            _model.CatalogUserGroups.ChangeGroupName(e.UserGroup, e.NewGroupName);
        }

        private void UnSignViewEvents()
        {
            _view.ButtonNewGroupClick -= _view_ButtonNewGroupClick;
            _view.ButtonEditUserGroupClick -= _view_ButtonCorrectUserGroupClick;
        }

        public void Dispose()
        {
            UnSignViewEvents();
        }
    }
}
