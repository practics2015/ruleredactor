﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Autentification;
using DevExpress.XtraTreeList;


namespace RuleRedactor
{
    public partial class RedactorForm : Form, IView
    {

        public RedactorForm()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                OnButtonOpenClick(folderBrowserDialog1.SelectedPath);
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                OnButtonSaveClick(folderBrowserDialog1.SelectedPath);
            }
        }   
        
        protected virtual void OnButtonSaveClick(string folderPath) 
        {
            PathEventArgs args;
            args = new PathEventArgs(folderPath);
            var handler = ButtonSaveClick;
            if (handler != null)
            {
                handler(this, args);
            }
        }
 
       
        private void RedactorForm_Load(object sender, EventArgs e)
        {

        }




        ///////////////////////Готовые методы

        public void SetUsers(ICollection<User> users)
        {
            foreach (var user in users)
            {
                var nodeUser = tlUsers.AppendNode(new object[] { user.SamName, user.DisplayName }, null);
                nodeUser.Tag = user;
                foreach (var role in user.Roles)
                {
                    var nodeRole = tlUsers.AppendNode(new object[] { role.Name }, nodeUser);
                    nodeRole.Tag = role;
                }
                foreach (var group in user.UserGroups)
                {
                    var nodeRole = tlUsers.AppendNode(new object[] { group.Name }, nodeUser);
                    nodeRole.Tag = group;
                }
            }
            // User userX = new User();
            // userX.SamName = "xxx";
            // userX.DisplayName = "xxx2";
            // var z = treeList1.AppendNode(new object[] { userX.SamName, userX.DisplayName }, null);
            // treeList1.AppendNode(new object[] { "role1" }, z);
            // treeList1.AppendNode(new object[] { "role2" }, z);
            //label1.Text = treeList1.FocusedNode.GetValue(0).ToString();
            // treeList1.FocusedNode.SetValue(0, "ss");
            // var UserX = treeList1.AppendNode(new object[] { user }, null);
            //var obj = treeList1.GetDataRecordByNode(node);

            //var user = obj as User;

            //if (user != null)
            //{sfghfz


            //}
            //treeList1.FocusedNode.SetValue(0,"ff");
        }

        public void SetUsersGroups(ICollection<UserGroup> userGroups)
        {
            foreach (var group in userGroups)
            {
                var nodeGroup = tlGroups.AppendNode(new object[] { group.Name }, null);
                nodeGroup.Tag = group;
                foreach (var user in group.Users)
                {
                    var nodeUser = tlGroups.AppendNode(new object[] { user.SamName }, nodeGroup);
                    nodeUser.Tag = user;
                }
            }
        }

        public void SetRoles(ICollection<Role> roles)
        {
            foreach (var role in roles)
            {
                var nodeRole = tlRoles.AppendNode(new object[] { role.Name }, null);
                nodeRole.Tag = role;
                foreach (var rule in role.Rules)
                {
                    var nodeRule = tlRoles.AppendNode(new object[] { rule.Name }, nodeRole);
                    nodeRule.Tag = rule;
                }
            }
        }

        public void SetRules(ICollection<Core.Autentification.Rule> rules)
        {
            foreach (var rule in rules)
            {
                var nodeRule = tlRules.AppendNode(new object[] { rule.Name, rule.Description }, null);
                nodeRule.Tag = rule;
            }
        }

        public void RefreshTreeData()
        {
            tlUsers.ClearNodes();
            tlGroups.ClearNodes();
            tlRoles.ClearNodes();
            tlRules.ClearNodes();
            var handler = RefreshData;

            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }




        protected virtual void OnButtonOpenClick(string folderPath)
        {
            tlUsers.ClearNodes();
            tlGroups.ClearNodes();
            tlRoles.ClearNodes();
            tlRules.ClearNodes();

            PathEventArgs args;
            var handler = ButtonLoadClick;
            args = new PathEventArgs(folderPath);
            if (handler != null)
            {
                handler(this, args);
            }
        }

        //Для пользователя!!!!

        private void butNewUser_Click(object sender, EventArgs e)
        {
            OnButtonNewUserClick();
        }

        private void butDeleteUser_Click(object sender, EventArgs e)
        {
            OnButtonDeleteUserClick();
        }

        private void butAddUserToGroup_Click(object sender, EventArgs e)
        {
            OnButtonAddUserToGroupClick();                                  //  добавить Пользователя в группу
        }

        private void butDeleteUserFromGroup_Click(object sender, EventArgs e)
        {
            OnButtonDeleteUserFromGroupClick();                             //    удалить пользователя из группы
        }

        private void butEditUser_Click(object sender, EventArgs e)
        {
            OnButtonEditUserClick();
        }

        private void OnButtonNewUserClick()
        {
            createOrEdit = false;
            EditUser UserForm = new EditUser();
            UserForm.Owner = this;
            userForEditForm = null;
            UserForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckUser())
                {
                    MessageBox.Show("Уже существует пользователь с таким логином");
                    return;
                }
                UserEventArgs args;
                var handler = ButtonNewUserClick;
                User nu = new User();
                nu.SamName = objName;
                nu.DisplayName = objDescription;
                args = new UserEventArgs(nu);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }

        }

        protected virtual void OnButtonDeleteUserClick()
        {
            UsersEventArgs args;
            var handler = ButtonDeleteUserClick;
            List<User> users = new List<User>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlUsers.Selection)
            {
                if (node.ParentNode == null)
                    users.Add((User)node.Tag);
                else return;
            }
            args = new UsersEventArgs(users);
            if (handler != null)
            {
                handler(this, args);
            }
            RefreshTreeData();
        }

        protected virtual void OnButtonAddUserToGroupClick()
        {
            UsersGroupsEventArgs args;
            var handler = ButtonAddUserToGroupClick;
            List<User> users = new List<User>();
            List<UserGroup> groups = new List<UserGroup>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlUsers.Selection)
                users.Add((User)selectNode.Tag);

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlGroups.Selection)
                groups.Add((UserGroup)selectNode.Tag);

            args = new UsersGroupsEventArgs(users, groups);
            if (handler != null)
            {
                handler(this, args);
            }
            
            RefreshTreeData();
        }

        protected virtual void OnButtonDeleteUserFromGroupClick()
        {
            UsersGroupsEventArgs args;
            var handler = ButtonRemoveUserFromGroupClick;
            List<User> users = new List<User>();
            List<UserGroup> groups = new List<UserGroup>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlUsers.Selection)
                users.Add((User)selectNode.Tag);

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlGroups.Selection)
                groups.Add((UserGroup)selectNode.Tag);

            args = new UsersGroupsEventArgs(users, groups);
            if (handler != null)
            {
                handler(this, args);
            }

            RefreshTreeData();
        }

        protected virtual void OnButtonEditUserClick()
        {

            if (tlUsers.Selection.Count != 1)
            {
                MessageBox.Show("Должен быть выделен только один пользователь");
                return;
            }
            if (tlUsers.Selection[0].ParentNode == null)
            {
                userForEditForm = (User)tlUsers.Selection[0].Tag;
            }
            else return;

            createOrEdit = false;
            EditUser UserForm = new EditUser();
            
            UserForm.Owner = this;
            
            UserForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckUser())
                {
                    MessageBox.Show("Уже существует пользователь с таким логином");
                    return;
                }
                UserAndNameEventArgs args;
                var handler = ButtonEditUserClick;

                    args = new UserAndNameEventArgs(userForEditForm, objName, objDescription);
                    if (handler != null)
                    {
                        handler(this, args);
                    }
                    RefreshTreeData();
            }

        }

        /// <summary>
        /// Пользователь, значения имени, которого мы изменяем
        /// </summary>
        User userForEditForm;
        public User UserForEditForm
        { get { return userForEditForm; } }

        /// <summary>
        /// Проверяет существет ли пользователь с таким логином
        /// </summary>
        /// <returns></returns>
        bool CheckUser()
        {
            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlUsers.Nodes)
            {
                User u = (User)node.Tag;
                if (u.SamName == objName)
                {
                    return false;
                }
            }
            return true;

        }

        // Для групп пользователей !!!!!

        private void butNewGroup_Click(object sender, EventArgs e)
        {
            OnButtonNewGroupClick();
        }

        private void butAddRoleToGroup_Click(object sender, EventArgs e)
        {
            OnButtonAddRoleToGroupClick();
        }

        private void butDeleteGroup_Click(object sender, EventArgs e)
        {
            OnButtonDeleteGroupClick();
        }

        private void butEditGroup_Click(object sender, EventArgs e)
        {
            OnButtonEditUserGroupClick();
        }

        protected virtual void OnButtonNewGroupClick()
        {
            createOrEdit = false;
            EditGroup GroupForm = new EditGroup();
            GroupForm.Owner = this;
            userForEditForm = null;
            GroupForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckGroup())
                {
                    MessageBox.Show("Уже существует группа с таким названием");
                    return;
                }
                GroupEventArgs args;
                var handler = ButtonNewGroupClick;
                UserGroup ng = new UserGroup();
                ng.Name = objName;
                
                args = new GroupEventArgs(ng);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }
        }

        protected virtual void OnButtonDeleteGroupClick()
        {
            GroupsEventArgs args;
            var handler = ButtonDeleteGroupClick;
            List<UserGroup> groups = new List<UserGroup>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlGroups.Selection)
            {
                if (node.ParentNode == null)
                    groups.Add((UserGroup)node.Tag);
                else return;
            }

            args = new GroupsEventArgs(groups);
            if (handler != null)
            {
                handler(this, args);
            }
            RefreshTreeData();
        }

        protected virtual void OnButtonAddRoleToGroupClick()
        {
            RolesGroupsEventArgs args;
            var handler = ButtonAddRoleToGroupClick;
            List<UserGroup> groups = new List<UserGroup>();
            List<Role> roles = new List<Role>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlGroups.Selection)
                groups.Add((UserGroup)selectNode.Tag);

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlRoles.Selection)
                roles.Add((Role)selectNode.Tag);

            args = new RolesGroupsEventArgs(roles, groups);
            if (handler != null)
            {
                handler(this, args);
            }

            RefreshTreeData();
        }

        protected virtual void OnButtonEditUserGroupClick()
        {
            if (tlGroups.Selection.Count != 1)
            {
                MessageBox.Show("Должена быть выделена только одна группа");
                return;
            }
            if (tlGroups.Selection[0].ParentNode == null)
            {
                groupForEditForm = (UserGroup)tlGroups.Selection[0].Tag;
            }
            else return;

            createOrEdit = false;
            EditGroup GroupForm = new EditGroup();
            
            GroupForm.Owner = this;

            GroupForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckGroup())
                {
                    MessageBox.Show("Уже существует группа с таким названием");
                    return;
                }
                GroupAndNameEventArgs args;
                var handler = ButtonEditUserGroupClick;

                args = new GroupAndNameEventArgs(groupForEditForm, objName);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }
        }

        /// <summary>
        /// Группа, название, которой мы изменяем
        /// </summary>
        UserGroup groupForEditForm;
        public UserGroup GroupForEditForm
        { get { return groupForEditForm; } }

        /// <summary>
        /// Проверяет существет ли группа с таким названем
        /// </summary>
        /// <returns></returns>
        bool CheckGroup()
        {
            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlGroups.Nodes)
            {
                UserGroup group = (UserGroup)node.Tag;
                if (group.Name == objName)
                {
                    return false;
                }
            }
            return true;

        }

        //Для Ролей!!!!!

        private void butNewRole_Click(object sender, EventArgs e)
        {
            OnButtonNewRoleClick();
        }
        private void butDeleteRole_Click(object sender, EventArgs e)
        {
            OnButtonDeleteRoleClick();
        }
        private void butAddRuleToRole_Click(object sender, EventArgs e)
        {
            OnButtonAddRuleToRoleClick();
        }
        private void butEditRole_Click(object sender, EventArgs e)
        {
            OnButtonEditRoleClick();
        }

        protected virtual void OnButtonNewRoleClick()
        {
            createOrEdit = false;
            EditRole RoleForm = new EditRole();
            RoleForm.Owner = this;
            roleForEditForm = null;
            RoleForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckRole())
                {
                    MessageBox.Show("Уже существует роль с таким названием");
                    return;
                }
                RoleEventArgs args;
                var handler = ButtonNewRoleClick;
                Role nr = new Role();
                nr.Name = objName;
                args = new RoleEventArgs(nr);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }
        }

        protected virtual void OnButtonDeleteRoleClick()
        {
            RolesEventArgs args;
            var handler = ButtonDeleteRoleClick;
            List<Role> roles = new List<Role>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlRoles.Selection)
            {
                if (node.ParentNode == null)
                    roles.Add((Role)node.Tag);
                else return;
            }

            args = new RolesEventArgs(roles);
            if (handler != null)
            {
                handler(this, args);
            }
            RefreshTreeData();
        }

        protected virtual void OnButtonAddRoleToUserClick()
        {
            
        }

        protected virtual void OnButtonAddRuleToRoleClick()
        {
            RulesRolesEventArgs args;
            var handler = ButtonAddRuleToRoleClick;
            List<Core.Autentification.Rule> rules = new List<Core.Autentification.Rule>();
            List<Role> roles = new List<Role>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlRoles.Selection)
                roles.Add((Role)selectNode.Tag);

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode selectNode in tlRules.Selection)
                rules.Add((Core.Autentification.Rule)selectNode.Tag);

            args = new RulesRolesEventArgs(rules, roles);
            if (handler != null)
            {
                handler(this, args);
            }

            RefreshTreeData();
        }

        protected virtual void OnButtonEditRoleClick()
        {
            if (tlRoles.Selection.Count != 1)
            {
                MessageBox.Show("Должена быть выделена только одна группа");
                return;
            }
            if (tlRoles.Selection[0].ParentNode == null)
            {
                roleForEditForm = (Role)tlRoles.Selection[0].Tag;
            }
            else return;

            createOrEdit = false;
            EditRole RoleForm = new EditRole();

            RoleForm.Owner = this;

            RoleForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckRole())
                {
                    MessageBox.Show("Уже существует роль с таким названием");
                    return;
                }
                RoleAndNameEventArgs args;
                var handler = ButtonEditRoleClick;

                args = new RoleAndNameEventArgs(roleForEditForm, objName);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }
        }

        /// <summary>
        /// Роль, название, которой мы изменяем
        /// </summary>
        Role roleForEditForm;
        public Role RoleForEditForm
        { get { return roleForEditForm; } }

        bool CheckRole()
        {
            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlRoles.Nodes)
            {
                Core.Autentification.Role role = (Role)node.Tag;
                if (role.Name == objName)
                {
                    return false;
                }
            }
            return true;

        }
     

        //Для Прав!!

        private void butNewRule_Click(object sender, EventArgs e)
        {
            OnButtonNewRuleClick();
        }

        private void butDeleteRule_Click(object sender, EventArgs e)
        {
            OnButtonDeleteRuleClick();
        }

        private void butEditRule_Click(object sender, EventArgs e)
        {
            OnButtonEditRuleClick();
        }

        protected virtual void OnButtonNewRuleClick()
        {
            createOrEdit = false;
            EditRule RuleForm = new EditRule();
            RuleForm.Owner = this;
            ruleForEditForm = null;
            RuleForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckRule())
                {
                    MessageBox.Show("Уже существует право с таким названием");
                    return;
                }
                RuleEventArgs args;
                var handler = ButtonNewRuleClick;
                Core.Autentification.Rule nru = new Core.Autentification.Rule();
                nru.Name = objName;
                nru.Description = objDescription;
                args = new RuleEventArgs(nru);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }
        }

        protected virtual void OnButtonDeleteRuleClick()
        {
            RulesEventArgs args;
            var handler = ButtonDeleteRuleClick;
            List<Core.Autentification.Rule> rules = new List<Core.Autentification.Rule>();

            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlRules.Selection)
            {
                if (node.ParentNode == null)
                    rules.Add((Core.Autentification.Rule)node.Tag);
                else return;
            }

            args = new RulesEventArgs(rules);
            if (handler != null)
            {
                handler(this, args);
            }
            RefreshTreeData();
        }

        protected virtual void OnButtonEditRuleClick() 
        {
            if (tlRules.Selection.Count != 1)
            {
                MessageBox.Show("Должено быть выделено только одно право");
                return;
            }
            if (tlRules.Selection[0].ParentNode == null)
            {
                ruleForEditForm = (Core.Autentification.Rule)tlRules.Selection[0].Tag;
            }
            else return;

            createOrEdit = false;
            EditRule RuleForm = new EditRule();

            RuleForm.Owner = this;

            RuleForm.ShowDialog();
            if (createOrEdit)
            {
                if (!CheckRule())
                {
                    MessageBox.Show("Уже существует право с таким названием");
                    return;
                }
                RuleAndNameEventArgs args;
                var handler = ButtonEditRuleClick;

                args = new RuleAndNameEventArgs(ruleForEditForm, objName, objDescription);
                if (handler != null)
                {
                    handler(this, args);
                }
                RefreshTreeData();
            }
        }


        /// <summary>
        /// Право, название, которого мы изменяем
        /// </summary>
        Core.Autentification.Rule ruleForEditForm;
        public Core.Autentification.Rule RuleForEditForm
        { get { return ruleForEditForm; } }

        bool CheckRule()
        {
            foreach (DevExpress.XtraTreeList.Nodes.TreeListNode node in tlRules.Nodes)
            {
                Core.Autentification.Rule rule = (Core.Autentification.Rule)node.Tag;
                if (rule.Name == objName)
                {
                    return false;
                }
            }
            return true;

        }

        //Общее!

        /// <summary>
        /// Переменная для определения было ли вызвано удаление или изменение объекта
        /// </summary>
        bool createOrEdit;
        public bool CreateOrEdit { set { createOrEdit = value; } }


        /// <summary>
        /// Имя нового или изменяемого объекта
        /// </summary>
        string objName;
        public string ObjName
        { set { objName = value; } }

        /// <summary>
        /// Описание нового или изменяемого пользователя
        /// </summary>
        string objDescription;
        public string ObjDescription
        { set { objDescription = value; } }
        
      
        
        //События
        
        public event EventHandler<PathEventArgs> ButtonLoadClick;

        public event EventHandler<PathEventArgs> ButtonSaveClick;

        public event EventHandler<UsersEventArgs> ButtonDeleteUserClick;

        public event EventHandler<GroupsEventArgs> ButtonDeleteGroupClick;

        public event EventHandler<RulesEventArgs> ButtonDeleteRuleClick;

        public event EventHandler<RolesEventArgs> ButtonDeleteRoleClick;

        public event EventHandler<UsersGroupsEventArgs> ButtonAddUserToGroupClick;

        public event EventHandler<RolesGroupsEventArgs> ButtonAddRoleToGroupClick;

        public event EventHandler<RolesUsersEventArgs> ButtonAddRoleToUserClick;

        public event EventHandler<RulesRolesEventArgs> ButtonAddRuleToRoleClick;

        public event EventHandler<UsersGroupsEventArgs> ButtonRemoveUserFromGroupClick;

        public event EventHandler<RulesRolesEventArgs> ButtonRemoveRuleFromRoleClick;

        public event EventHandler<RolesUsersEventArgs> ButtonRemoveRoleFromUserClick;

        public event EventHandler RefreshData;

        public event EventHandler<RolesGroupsEventArgs> ButtonRemoveRoleFromGroupClick;

        public event EventHandler<UserEventArgs> ButtonNewUserClick;

        public event EventHandler<UserAndNameEventArgs> ButtonEditUserClick;
        
        public event EventHandler<GroupEventArgs> ButtonNewGroupClick;
        
        public event EventHandler<GroupAndNameEventArgs> ButtonEditUserGroupClick;

        public event EventHandler<RoleEventArgs> ButtonNewRoleClick;

        public event EventHandler<RoleAndNameEventArgs> ButtonEditRoleClick;

        public event EventHandler<RuleEventArgs> ButtonNewRuleClick;

        public event EventHandler<RuleAndNameEventArgs> ButtonEditRuleClick;

        /// <summary>
        /// Удаление при нажатии кнопки Delete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tlUsers_KeyDown(object sender, KeyEventArgs e)
        {
            if ((int)e.KeyCode == 46)
                OnButtonDeleteUserClick();
        }

        private void tlGroups_KeyDown(object sender, KeyEventArgs e)
        {
            if ((int)e.KeyCode == 46)
                OnButtonDeleteGroupClick();
        }

        private void tlRoles_KeyDown(object sender, KeyEventArgs e)
        {
            if ((int)e.KeyCode == 46)
                OnButtonDeleteRoleClick();
        }

        private void tlRules_KeyDown(object sender, KeyEventArgs e)
        {
            if ((int)e.KeyCode == 46)
                OnButtonDeleteRuleClick();
        }
    }
}