﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Autentification;

namespace RuleRedactor
{
    public partial class EditRole : Form, IViewRoles
    {
        public EditRole()
        {
            InitializeComponent();
        }

        RedactorForm main;

        public event EventHandler<RoleEventArgs> ButtonNewRoleClick;

        public event EventHandler<RoleAndNameEventArgs> ButtonEditRoleClick;

        private void butOK_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Должно быть название роли!!!");
                return;
            }
            if (main != null)
            {
                main.CreateOrEdit = true;
                main.ObjName = textBox1.Text;

            }
            this.Close();
        }

        private void EditRole_Load(object sender, EventArgs e)
        {
            main = this.Owner as RedactorForm;
            if (main != null && main.RoleForEditForm != null)
            {
                textBox1.Text = main.RoleForEditForm.Name;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
