﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RuleRedactor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            
            var view = new RedactorForm();
            var viewUsers = new EditUser();
            var viewGroups = new EditGroup();
            var viewRules = new EditRule();
            var viewRoles = new EditRole();

            var model = new UserManager();
            
            var presenter = new Presenter(model, view);
            var presUsers = new PresenterUsers(model,viewUsers);
            var presGroups = new PresenterGroups(model, viewGroups);
            var presRules = new PresenterRules(model, viewRules);
            var presRoles = new PresenterRoles(model, viewRoles);

            Application.Run(view);
            //Application.Run(new RedactorForm());
            //Application.Run(viewUsers);
        }
    }
}
