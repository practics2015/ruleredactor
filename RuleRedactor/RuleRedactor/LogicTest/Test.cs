﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Core.Autentification;

namespace RuleRedactor.LogicTest
{
    [TestFixture]
    class Test
    {

        User user1 = new User() { SamName = "User_1", DisplayName = "Display User_1" };
        User user2 = new User() { SamName = "User_2", DisplayName = "Display User_2" };
        User user3 = new User() { SamName = "User_3", DisplayName = "Display User_3" };
        User user4 = new User() { SamName = "User_4", DisplayName = "Display User_4" };
        
        UserGroup userGroup1 = new UserGroup() { Name = "UserGroup_1" };
        UserGroup userGroup2 = new UserGroup() { Name = "UserGroup_2" };

        Rule rule1 = new Rule() { Name = "Rule_1", Description = "Description Rule_1" };
        Rule rule2 = new Rule() { Name = "Rule_2", Description = "Description Rule_2" };
        Rule rule3 = new Rule() { Name = "Rule_3", Description = "Description Rule_3" };
        Rule rule4 = new Rule() { Name = "Rule_4", Description = "Description Rule_4" };

        Role role1 = new Role() { Name = "Role_1" };
        Role role2 = new Role() { Name = "Role_2" };

        CatalogUsers catUser;
        CatalogUserGroups catUserGroups;
        CatalogRules catRules;
        CatalogRoles catRoles;

        private void CreateTestData()
        {            
            userGroup1.Users.Add(user1);
            user1.UserGroups.Add(userGroup1);
            
            userGroup2.Users.Add(user2);
            user2.UserGroups.Add(userGroup2);
         
            role1.Rules.Add(rule1);
            role2.Rules.Add(rule2);

            CatalogUsers catUser = new CatalogUsers() { Users = new List<User>() { user1, user2, user3, user4 } };
            CatalogUserGroups catUserGroups = new CatalogUserGroups() { Groups = new List<UserGroup>() { userGroup1, userGroup2 } };
            CatalogRules catRules = new CatalogRules() { Rules = new List<Rule>() { rule1, rule2, rule3, rule4 } };
            CatalogRoles catRoles = new CatalogRoles() { Roles = new List<Role>() { role1, role2 } };
            
        }

        //[Test]
        //public void Test1_RunTask()
        //{
        //    var userManager = new UserManager();
            
        //    userManager.LoadCatalogs(@"c:\Users\Nipi_049\AppData\Roaming\ServerApplication\ServerApplication\1.0.0.0\Core.Autentification.CatalogUsers.xml");

        //    Assert.True(userManager.CatalogUsers.Users.Count > 0);
        //}

        public Test()
        {
            CreateTestData();
        }

        //[Test]
        //public void Test1_RunTask()
        //{
        //    var userManager = new UserManager();

        //    userManager.LoadCatalogs(@"c:\Users\Nipi_049\AppData\Roaming\ServerApplication\ServerApplication\1.0.0.0\Core.Autentification.CatalogUsers.xml");

        //    Assert.True(userManager.CatalogUsers.Users.Count > 0);
        //}

        [Test]
        public void TestEqulity()
        {
            //CreateTestData();
            User User = new User() { SamName = "User_1", DisplayName = "I'm NewUser" };
            Assert.True(user1.Equals(User));
            //Assert.True(catUser.Users.Contains(User));
            UserGroup Group = new UserGroup() { Name = "UserGroup_1" };
            Assert.True(userGroup1.Equals(Group));
            //Assert.True(catUserGroups.Groups.Contains(Group));
            Role Role = new Role() { Name = "Role_1" };
            Assert.True(role1.Equals(Role));
            // Assert.True(catRoles.Roles.Contains(Role));
            Rule Rule = new Rule() { Name = "Rule_1" };
            Assert.True(rule1.Equals(Rule));
            // Assert.True(catRules.Rules.Contains(Rule));
        }

        [Test]
        public void TestNewUser()
        {
            //CreateTestData();

            User newUser = new User() { SamName = "newUser", DisplayName = "I'm NewUser" };
            catUser.AddUser(newUser);
            Assert.True(catUser.Users.Contains(newUser));
            int count_1 = catUser.Users.Count;
            //тут вместо этого можно добавит проверку на получение Exception
            catUser.AddUser(newUser);
            int count_2 = catUser.Users.Count;
            Assert.True(count_2 - count_1 == 0);

        }

        [Test]
        public void TestDeleteUser()
        {
            //CreateTestData();
            User anotherUser = new User() { SamName = "User_1", DisplayName = "anothUser" };


            catUser.RemoveUser(user1);
            Assert.False(catUser.Users.Contains(user1));

            int count_1 = catUser.Users.Count;
            catUser.RemoveUser(anotherUser);
            int count_2 = catUser.Users.Count;
            Assert.True(count_2 == count_1);
        }

        [Test]
        public void TestAddUserToGroup()
        {
            //Добавился ли пользователь в группу
            //CreateTestData();
            userGroup1.AddUser(user2);
            Assert.True(userGroup1.Users.Contains(user2));
            int count_1 = userGroup1.Users.Count;
            userGroup1.AddUser(user2);
            int count_2 = userGroup1.Users.Count;
            Assert.True(count_2 == count_1);

            //Добавилась ли группа в список групп пользователя
            user2.AddUserGroup(userGroup1);
            Assert.True(user2.UserGroups.Contains(userGroup1));
            int count_3 = user2.UserGroups.Count;
            userGroup1.AddUser(user2);
            int count_4 = user2.UserGroups.Count;
            Assert.True(count_3 == count_4);

        }

        [Test]
        public void TestRemoveUserFromGroup()
        {
            //CreateTestData();
            Assert.True(userGroup1.Users.Contains(user1));
            userGroup1.RemoveUser(user1);

            //User nu = new User() { SamName = "xxx"};
            //userGroup1.AddUser(nu);
            //userGroup1.RemoveUser(nu);
            //Assert.False(userGroup1.Users.Contains(nu));

            userGroup1.RemoveUser(user1);
            Assert.False(userGroup1.Users.Contains(user1));

            int count_1 = userGroup1.Users.Count;
            userGroup1.RemoveUser(user4);
            int count_2 = userGroup1.Users.Count;
            Assert.True(count_2 == count_1);

            Assert.True(user1.UserGroups.Contains(userGroup1));

            user1.RemoveUserGroup(userGroup1);

            Assert.False(user1.UserGroups.Contains(userGroup1));

            int count_3 = user2.UserGroups.Count;
            userGroup1.RemoveUser(user2);
            int count_4 = user2.UserGroups.Count;
            Assert.True(count_3 == count_4);
        }

        [Test]
        public void TestChangeUserName()
        {
            User anU = new User() { SamName = "My first name", DisplayName = "My first dis" };
            catUser.Users.Add(anU);
            string newSamName = "new name";
            string newDisplayName = "ndqwe";
            catUser.ChangeUserNames(anU, newSamName, newDisplayName);
            Assert.True(anU.SamName == newSamName);
            Assert.True(anU.DisplayName == newDisplayName);
        }

    }
}
