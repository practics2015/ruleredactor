﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Autentification;

namespace RuleRedactor
{
    public partial class EditGroup : Form, IViewGroups
    {
        public EditGroup()
        {
            InitializeComponent();
        }

        RedactorForm main;

        private void butOK_Click(object sender, EventArgs e)
        {
            OnButtonOKClick();
        }


        private void EditGroup_Load(object sender, EventArgs e)
        {
            main = this.Owner as RedactorForm;
            if (main != null && main.GroupForEditForm != null)
            {
                textBox1.Text = main.GroupForEditForm.Name;
            }
        }

        protected virtual void OnButtonOKClick()
        {
            //var handler = ButtonNewUserClick;
            if (textBox1.Text == "")
            {
                MessageBox.Show("Должно быть название группы!!!");
                return;
            }
            if (main != null)
            {
                main.CreateOrEdit = true;
                main.ObjName = textBox1.Text;

            }
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public event EventHandler<GroupEventArgs> ButtonNewGroupClick;

        public event EventHandler<GroupAndNameEventArgs> ButtonEditUserGroupClick;
    }
}
